import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "../pages/HomePage";
import SearchPage from "../pages/SearchPage";
import HomeContentPage from "../pages/HomeContentPage";
import NoMatchPage from "../pages/NoMatchPage";
import ROUTER, { LIKED_SONGS, TOP_LISTEN } from "../utils/constants/router.constants.js";
import SearchContentPage from "../pages/SearchContentPage";
import SearchTextContentPage from "../pages/SearchTextContentPage";
import GenrePage from "../pages/GenrePage";
import AlbumPlayListPage from "../pages/AlbumPlayListPage";
import useGoogleLogin from "react-google-login/src/use-google-login";
import { useRecoilCallback, useRecoilState, useSetRecoilState } from "recoil";
import {
  authConfig,
  handleAfterAuth,
  musixToken,
} from "../utils/auth.utils.js";
import { useEffect } from "react";
import { musixAxios } from "../utils/axios.utils.js";
import { authState } from "../atoms/auth.atoms.js";
import ArtistPage from "../pages/ArtistPage";
import { likedItemsSelectorState } from "../selector/albumPlayList.selector.js";
import { likedItemsState } from "../atoms/albumPlayList.atom.js";
import QueuePage from "../pages/QueuePage";
import { uAgentState } from "../hooks/useAgent";
import PrivacyPage from "../pages/PrivacyPage";

export default function App() {
  const [auth, setAuth] = useRecoilState(authState);
  const setUAgent = useSetRecoilState(uAgentState);
  const setLikedItems = useRecoilCallback(
    ({ set, snapshot }) =>
      async () => {
        try {
          const rows = await snapshot.getPromise(likedItemsSelectorState);
          set(likedItemsState, rows);
        } catch (e) {
          console.error(e);
        }
      },
    []
  );
  const handleSuccess = async (resp) => {
    if (resp) {
      const {
        accessToken,
        profileObj: { email, googleId, imageUrl, name },
      } = resp;
      try {
        const resp2 = await musixAxios(musixToken()).post("/users/", {
          email,
          imageUrl,
          providerId: googleId,
          userName: name,
        });
        if (resp2.data.success) {
          setAuth((prevState) => ({ ...prevState, isAuth: true }));
          handleAfterAuth(accessToken, email);
          setLikedItems();
        }
      } catch (e) {
        setAuth((prevState) => ({ ...prevState, isAuth: false }));
      }
    }
  };

  const handleFailure = (resp) => {
    console.error(resp);
  };

  const handleAutoLoadFinished = (isSignedIn: boolean) => {
    if (!isSignedIn) setAuth((prevState) => ({ ...prevState, isAuth: false }));
  };
  const { signIn, loaded } = useGoogleLogin({
    ...authConfig,
    uxMode: "redirect",
    autoLoad: false,
    onSuccess: handleSuccess,
    onFailure: handleFailure,
    onScriptLoadFailure: handleFailure,
    isSignedIn: true,
    accessType: "offline",
    onAutoLoadFinished: handleAutoLoadFinished,
  });

  useEffect(() => {
    if (loaded) {
      setAuth((prevState) => ({ ...prevState, signIn }));
      setUAgent(navigator.userAgent.includes("Mobile"));
    }
  }, [loaded]);
  if (auth.isAuth !== -1)
    return (
      <Router basename={"/"}>
        <Routes>
          <Route path={`${ROUTER.HOME}*`} element={<HomePage />}>
            <Route index element={<HomeContentPage />} />
            <Route path={ROUTER.SEARCH} element={<SearchPage />}>
              <Route index element={<SearchContentPage />} />
              <Route path=":searchText" element={<SearchTextContentPage />} />
            </Route>
            <Route path={`${ROUTER.GENRE}/:property`} element={<GenrePage />} />
            <Route path={"recommend/:property"} element={<GenrePage />} />
            <Route
              path={`${ROUTER.ALBUM}/:albumId`}
              element={<AlbumPlayListPage />}
            />
            <Route
              path={`${ROUTER.PLAYLIST}/:playlistId`}
              element={<AlbumPlayListPage />}
            />
            <Route
              path={`${ROUTER.UPLAYLIST}/:playlistId`}
              element={<AlbumPlayListPage />}
            />
            <Route path={ROUTER.COLLECTIONS.setSubRoute(LIKED_SONGS)} element={<AlbumPlayListPage />} />
            <Route path={ROUTER.COLLECTIONS.setSubRoute(TOP_LISTEN)} element={<AlbumPlayListPage />} />
            <Route
              path={`${ROUTER.ARTIST}/:artistId`}
              element={<ArtistPage />}
            />
            <Route path={`${ROUTER.QUEUE}`} element={<QueuePage />} />
            <Route path="privacy" element={<PrivacyPage />} />
            <Route path="*" element={<NoMatchPage />} />
          </Route>
        </Routes>
      </Router>
    );
  return null;
}
