import { DefaultValue, selector, selectorFamily, waitForAll } from "recoil";
import {
  albumPlayListTrackState,
  ModAlbumPlaylistSelType,
  searchTrackState,
} from "../atoms/albumPlayList.atom.js";
import { musixToken } from "../utils/auth.utils.js";
import { musixAxios } from "../utils/axios.utils.js";
import ROUTER, { LIKED_SONGS, TOP_LISTEN } from "../utils/constants/router.constants.js";
import { PLAYMODE } from "../utils/constants/trackState.constants.js";
import { searchTrackTemplate } from "../utils/conversion.utils.js";
import {
  getAlbum,
  getPlayListDetails,
  getTracks,
} from "../utils/spotify.utils.js";
import { artistInfoSelector } from "./artist.selector.js";
import {
  getRecommendState,
  paramsType,
  uPlaylistState,
} from "./content.selector.js";

export const albumPlayListSelectorTrackState = selector({
  key: "albumPlayListSelectorTrackState",
  get: ({ get }) => {
    const albumPlayListTrack = get(albumPlayListTrackState);
    const items = albumPlayListTrack?.items ?? [];
    const idx = items?.findIndex(
      (track) =>
        track.isPlaying === PLAYMODE.PLAYING ||
        track.isPlaying === PLAYMODE.PAUSE
    );
    if (idx >= 0) {
      const nextIdx = idx + 1 === items.length ? 0 : idx + 1;
      const prevIdx = idx - 1 === -1 ? items.length - 1 : idx - 1;
      const nextTrack = items[nextIdx];
      const prevTrack = items[prevIdx];
      return {
        currentItem: items[idx],
        nextIdx,
        prevIdx,
        currIdx: idx,
        totalLength: items.length,
        nextSearchTrack: searchTrackTemplate(
          nextTrack.song,
          nextTrack.artists[0].name
        ),
        prevSearchTrack: searchTrackTemplate(
          prevTrack.song,
          prevTrack.artists[0].name
        ),
      } as ModAlbumPlaylistSelType;
    }
    return {
      nextIdx: idx,
      nextSearchTrack: get(searchTrackState),
    } as ModAlbumPlaylistSelType;
  },
  set: ({ set, get }, trackInfo: ModAlbumPlaylistSelType | DefaultValue) => {
    if (!(trackInfo instanceof DefaultValue)) {
      const { id, searchTrack, type, albumPlayListId } = trackInfo;
      const albumPlayListDetails = get(
        albumPlayListDetailsSate({ type, id: albumPlayListId })
      );
      const items =
        albumPlayListDetails?.tracks?.items ||
        albumPlayListDetails?.tracks ||
        [];
      const modItems = items.map((item) => {
        const name = item?.track?.name || item?.name;
        const artists = item?.track?.artists || item?.artists;
        const itemId = item?.track?.id || item?.id;
        const linkedItemId = item?.linked_from?.id;
        const duration = item?.track?.duration_ms || item?.duration_ms;
        const album = item?.track?.album ||
          item?.album || {
          name: albumPlayListDetails.name,
          images: [{ url: albumPlayListDetails?.images?.[0]?.url }],
        };
        const imageSource = album?.images?.[0]?.url;
        return {
          itemId,
          imageSource,
          album,
          song: name,
          artists,
          duration,
          isPlaying:
            id === itemId || linkedItemId == id
              ? PLAYMODE.PLAYING
              : PLAYMODE.INQUEUE,
        };
      });
      set(searchTrackState, searchTrack);
      set(albumPlayListTrackState, {
        id: albumPlayListDetails.id,
        type,
        isPlaying: PLAYMODE.LOADING,
        items: modItems,
      });
    }
  },
});

const albumState = selectorFamily({
  key: "albumState",
  get: (id: string) => async () => {
    const [data, error] = await getAlbum(id, { market: "IN" });
    if (error) throw error;
    return data;
  },
});

const playlistState = selectorFamily({
  key: "playlistState",
  get: (id: string) => async () => {
    const [data, error] = await getPlayListDetails(id, {
      id,
      market: "IN",
      limit: 20,
    });
    if (error) throw error;
    return data;
  },
});
export const albumPlayListDetailsSate = selectorFamily({
  key: "albumPlayListDetailsState",
  get:
    (params: paramsType) =>
      async ({ get }) => {
        const { type, id } = params;
        if (type === ROUTER.ALBUM) return get(albumState(id));
        else if (type === ROUTER.PLAYLIST) return get(playlistState(id));
        else if (type === ROUTER.UPLAYLIST) {
          const data = get(uPlaylistState);
          const foundIdx = data?.rows.findIndex((ele) => ele.id === id);
          if (foundIdx > -1) {
            const dailyMix = data.rows[foundIdx];
            const { artistIds, genres, trackIds } = dailyMix;
            const items =
            get(
              getRecommendState({
                artistIds,
                genres,
                trackIds: trackIds.split(",").slice(0, 2).join(),
                limit: "20",
              })
            )?.tracks ?? [];
            return {
              ...dailyMix,
              tracks: items,
            };
          }
        } else if (type === ROUTER.COLLECTIONS.name) {
          if(id === LIKED_SONGS) {
            const rows = get(likedItemsSelectorState);
            const idArr = Object.entries(rows)
              .filter((row) => row[1] == "track")
              .map((row) => row[0]);
            const length = idArr.length;
            const joinIds: string[] = [];
            for(let i=0; i < Math.ceil(length/50); i++) {
              joinIds.push(idArr.slice(i * 50, (i + 1) * 50).join());
            }
            const tracks = get(
              waitForAll(
                joinIds.map((ids) => trackState(ids))
              )
            ).flat();
            const firstTrack = tracks[0] as {album: {images: Array<{url: string}>}};
            return {
              id,
              type,
              name: "Liked Songs",
              description: "",
              imgUrl: `${firstTrack.album?.images[0]?.url}`,
              tracks,
            };
          }else if(id === TOP_LISTEN) {
            const data = get(topListenSelectorState({page: 0, size: 50}));
            const length = data.length;
            const joinIds: string[] = [];
            for(let i=0; i < Math.ceil(length/50); i++) {
              joinIds.push(data.slice(i * 50, (i + 1) * 50).join());
            }
            const tracks = get(
              waitForAll(
                joinIds.map((ids) => trackState(ids))
              )
            ).flat();
            const firstTrack = tracks[0] as {album: {images: Array<{url: string}>}};
            return {
              id,
              type,
              name: "Top Listen Songs",
              description: "Play your songs that touch your heart",
              imgUrl: firstTrack.album?.images[0]?.url ?? '', 
              tracks
            };
          }
        } else if (type === ROUTER.ARTIST) {
          const data = get(artistInfoSelector(id));
          const tracks = data?.[1]?.[0]?.tracks ?? [];
          return { tracks, id };
        }
      },
});

export const likedItemsSelectorState = selector({
  key: "likedItemsSelectorState",
  get: async () => {
    try {
      const resp = await musixAxios(musixToken()).get("/liked/");
      return resp.data?.rows;
    } catch (e) {
      throw e;
    }
  },
});

export const trackState = selectorFamily({
  key: "trackState",
  get: (ids) => async () => {
    const [data, error] = await getTracks({ ids });
    if (error) throw undefined;
    return data?.tracks;
  },
});

export const topListenSelectorState = selectorFamily({
  key: "topListenSelectorState",
  get: ({page, size}: {page: number, size: number}) => async() => {
    try {
      const resp = await musixAxios(musixToken()).get(`/playItems/topListens?page=${page}&size=${size}`);
      return resp.data.rows;
    } catch (e) {
      throw e;
    }
  }
});