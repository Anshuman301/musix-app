import { selectorFamily } from "recoil";
import {
  getArtist,
  getArtistAlbums,
  getArtistTopTracks,
  getRelatedArtists,
} from "../utils/spotify.utils";

export const artistInfoSelector = selectorFamily({
  key: "artistInfoSelector",
  get: (artistId: string) => async () => {
    const data = await Promise.all([
      getArtist(artistId),
      getArtistTopTracks(artistId, { market: "IN" }),
      getArtistAlbums(artistId, {
        include_groups: "album",
        market: "IN",
        limit: 6,
      }),
      getRelatedArtists(artistId, { limit: 5 }),
    ]);
    return [...data];
  },
});
