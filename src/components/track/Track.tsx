import {
  Box,
  Grid,
  GridItem,
  HStack,
  Icon,
  Image,
  Text,
} from "@chakra-ui/react";
import { useState } from "react";
import { FaPause, FaPlay } from "react-icons/fa";
import { FcLike } from "react-icons/fc";
import { GiSelfLove } from "react-icons/gi";
import { useRecoilCallback, useRecoilState, useRecoilValue } from "recoil";
import {
  albumPlayListTrackState,
  ArtistType,
  likedItemsState,
  ModAlbumPlaylistSelType,
  searchTrackState,
} from "../../atoms/albumPlayList.atom.js";
import { authState } from "../../atoms/auth.atoms.js";
import useCustomToast from "../../hooks/useCustomToast.js";
import useLiked from "../../hooks/useLiked.js";
import usePlayPauseClick from "../../hooks/usePlayPauseClick.js";
import {
  albumPlayListDetailsSate,
  albumPlayListSelectorTrackState,
} from "../../selector/albumPlayList.selector.js";
import type { paramsType } from "../../selector/content.selector.js";
import { musixToken } from "../../utils/auth.utils.js";
import { musixAxios } from "../../utils/axios.utils.js";
import ROUTER from "../../utils/constants/router.constants.js";
import { PLAYMODE } from "../../utils/constants/trackState.constants.js";
import {
  getNewStateForPlayPause,
  searchTrackTemplate,
  secondsToMins,
} from "../../utils/conversion.utils.js";
import {
  pxToAll,
  pxToRem,
  pxToRemSm,
  showOut,
} from "../../utils/theme.utils.js";
import { getArtistLabel } from "../ContentWrapper/ContentWrapper.js";

export default function Track (props) {
  const {
    isPlaying: isAlbumPlayListPlaying,
    isLoading: isAlbumPlayListLoading,
    handlePauseClick,
  } = usePlayPauseClick(props.header.id);
  const [albumPlayListTrack, setAlbumPlayListTrack] = useRecoilState(
    albumPlayListTrackState
  );
  const albumPlayListSelectorTrack = useRecoilValue(
    albumPlayListSelectorTrackState
  );
  const auth = useRecoilValue(authState);
  const toast = useCustomToast();
  const onLiked = useLiked();
  const likedItems = useRecoilValue(likedItemsState);
  const [isEnter, setEnter] = useState(false);
  const currentItem = albumPlayListSelectorTrack?.currentItem;
  const nextIdx = albumPlayListSelectorTrack?.nextIdx;
  const totalLength = albumPlayListSelectorTrack?.totalLength;
  const isCurrentItem =
    currentItem?.itemId === props.id &&
    albumPlayListTrack?.id === props.header?.id;
  const isPlaying =
    isCurrentItem && currentItem?.isPlaying === PLAYMODE.PLAYING;
  const artists: ArtistType[] = props?.artists ?? [];
  const album = props?.album;
  const imageUrl = props?.imgUrl || album?.images?.[0]?.url;
  const { artistLabels, artistName } = { artistLabels: getArtistLabel(artists), artistName: artists.map(artist => artist.name) }
  const isLiked =
    !!likedItems?.[props?.id] && likedItems?.[props?.id] === "track";
  const handlePlayCallback = useRecoilCallback(
    ({ snapshot, set }) =>
      async (
        header,
        trackName,
        artistName,
        trackId: string,
        obj: { isCurrentItem: boolean; nextIdx: number; totalLength: number }
      ) => {
        const albumPlayListTrack = snapshot.getLoadable(
          albumPlayListTrackState
        );
        const release = snapshot.retain();
        try {
          const { type, id, name, imgUrl, ...rest } = header as paramsType;
          await snapshot.getPromise(albumPlayListDetailsSate({ type, id }));
          if (albumPlayListTrack.contents?.id !== id) {
            const resp = await musixAxios(musixToken()).put("/playItems/", {
              type,
              spotifyId: id,
              name,
              description: rest?.desc,
              imgUrl,
              artists: rest?.artists,
            });
            if (!(resp.status >= 400)) {
              set(albumPlayListSelectorTrackState, {
                id: trackId,
                searchTrack: searchTrackTemplate(trackName, artistName),
                type,
                albumPlayListId: id,
              } as ModAlbumPlaylistSelType);
            }
          } else {
            const searchTrackLoadable = snapshot.getLoadable(searchTrackState)
            const searchTrack = searchTrackLoadable.getValue()
            if (obj.isCurrentItem && searchTrack) {
              let currentIdx = obj.nextIdx - 1;
              if (nextIdx == 0) currentIdx = obj.totalLength - 1;
              setAlbumPlayListTrack((prevState) =>
                getNewStateForPlayPause(prevState, PLAYMODE.PLAYING, currentIdx)
              );
            } else
              set(albumPlayListSelectorTrackState, {
                id: trackId,
                searchTrack: searchTrackTemplate(trackName, artistName),
                type,
                albumPlayListId: id,
              } as ModAlbumPlaylistSelType);
          }
        } catch (e) {
          console.error(e);
        } finally {
          release();
        }
      },
    []
  );

  const handleClick = () => {
    if (auth.isAuth) {
      if (!isAlbumPlayListLoading) {
        if (isPlaying && isAlbumPlayListPlaying) handlePauseClick();
        else
          handlePlayCallback(
            props.header,
            props.name,
            artistName[0],
            props.id,
            { isCurrentItem, nextIdx, totalLength }
          );
      }
    } else toast();
  };
  return (
    <Grid
      templateColumns={
        imageUrl
          ? `25px minmax(170px, 2fr) minmax(85px, 1fr) 70px`
          : "25px minmax(190px, 1fr) 70px"
      }
      gridColumnGap={pxToAll(20)}
      height={pxToAll(70)}
      mt={pxToAll(10)}
      alignItems={"center"}
      px={pxToAll(20)}
      transition="all 0.25s"
      _hover={{
        bg: "shade.hoverPrimary",
        color: "text.secondary",
        borderRadius: "10px",
        transition: "all 0.25s",
      }}
      onClick={handleClick}
      onMouseEnter={() => setEnter(true)}
      onMouseLeave={() => setEnter(false)}
    >
      <GridItem justifySelf={"end"}>
        {isEnter ? (
          <Icon
            as={isPlaying ? FaPause : FaPlay}
            textStyle={"icon.md"}
            _hover={{
              color: "text.play",
              transition: "all 0.25s",
            }}
          />
        ) : (
          <Text textStyle={"h6"}>{props.seq}</Text>
        )}
      </GridItem>
      <GridItem>
        <HStack>
          {imageUrl && (
            <Box width={pxToAll(60)}>
              <Image
                src={imageUrl}
                animation={`${showOut} 0.5s linear 1 normal`}
              />
            </Box>
          )}
          <Box
            width={[
              `calc(100% - ${pxToRemSm(60 / 1.5)} - 0.5rem)`,
              null,
              `calc(100% - ${pxToRem(60)} - 0.5rem)`,
            ]}
          >
            <Text
              textStyle={"h6"}
              color={!isCurrentItem ? "text.secondary" : "text.play"}
              isTruncated
            >
              {props.name}
            </Text>
            <Text textStyle={"label"} isTruncated>
              {artistLabels}
            </Text>
          </Box>
        </HStack>
      </GridItem>
      {imageUrl && (
        <GridItem>
          <Text textStyle={"label"} isTruncated>
            {album?.name}
          </Text>
        </GridItem>
      )}
      <GridItem
        justifySelf={"end"}
        columnGap={pxToRem(40)}
        height={"100%"}
        alignItems="center"
        d={"flex"}
      >
        {isEnter && (
          <Icon
            as={isLiked ? FcLike : GiSelfLove}
            textStyle={"icon.md"}
            _hover={{
              color: "text.play",
              transition: "all 0.25s",
            }}
            onClick={(e) => {
              e.stopPropagation();
              if (auth.isAuth) onLiked(props.id, "track");
              else toast();
            }}
          />
        )}
        <Text textStyle={"label"}>
          {secondsToMins(props?.duration_ms / 1000)}
        </Text>
      </GridItem>
    </Grid>
  );
}
