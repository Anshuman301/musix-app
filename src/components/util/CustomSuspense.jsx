import { Fragment } from "react";

export default function CustomSuspense(props) {
  const { state, fallback, children, ...rest } = props;
  let component = children;
  if (state === "loading") component = fallback;
  return <Fragment children={component} {...rest} />;
}
