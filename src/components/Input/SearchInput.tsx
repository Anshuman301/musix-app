import {
  Icon,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from "@chakra-ui/react";
import { useEffect, useRef, useState } from "react";
import { MdClose, MdSearch } from "react-icons/md";
import { useLocation, useNavigate } from "react-router-dom";
import { selectorFamily } from "recoil";
import useAgent from "../../hooks/useAgent.js";
import type { paramsType } from "../../selector/content.selector.js";
import ROUTER from "../../utils/constants/router.constants.js";
import { debounce } from "../../utils/conversion.utils.js";
import { searchItems } from "../../utils/spotify.utils.js";

export const searchDetailsState = selectorFamily({
  key: "searchDetailsState",
  get: (searchText) => async () => {
    const [data, error] = await searchItems({
      q: searchText,
      type: "track,artist,album,playlist",
      limit: 5,
      country: "IN",
    });
    if (error) throw error;
    return data;
  },
});

export default function SearchInput({ ...rest }) {
  const state = useLocation().state as paramsType;
  const [modSearchText, setSearchText] = useState(() => state?.search ?? "");
  const navigate = useNavigate();
  const inputRef = useRef<HTMLInputElement>(null);
  const isMobile = useAgent();
  useEffect(() => {
    if (inputRef.current) if (!isMobile) inputRef.current.focus();
  }, []);

  useEffect(() => {
    navigate(`/${ROUTER.SEARCH}/${modSearchText}`, {
      replace: true,
      state: { search: modSearchText },
    });
  }, [modSearchText]);

  const handleChange = (value: string) => {
    setSearchText(value);
  };
  const debounceHandleChange = debounce(handleChange, 500);
  return (
    <InputGroup {...rest}>
      <InputLeftElement
        children={<Icon as={MdSearch} textStyle={"icon.md"} />}
      />
      <Input
        type={"text"}
        placeholder="Songs,artists,albums"
        aria-label="search"
        defaultValue={modSearchText}
        onChange={(e) => debounceHandleChange(e.target.value)}
        ref={inputRef}
      />
      <InputRightElement
        children={
          !!modSearchText ? <Icon as={MdClose} textStyle={"icon.md"} /> : null
        }
        onClick={() => {
          if (inputRef.current !== null) inputRef.current.value = "";
          setSearchText("");
        }}
      />
    </InputGroup>
  );
}
