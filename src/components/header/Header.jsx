import { Box, HStack, Button, Circle, Icon, Popover, PopoverTrigger, PopoverContent, PopoverArrow, PopoverBody, Image } from "@chakra-ui/react";
import { pxToAll } from "../../utils/theme.utils.js";
import Logo from "../logo/Logo";
import { Route, Routes } from "react-router-dom";
import ROUTER from "../../utils/constants/router.constants.js";
import AgentDetect from "../util/AgentDetect.jsx";
import SearchInput from "../Input/SearchInput.jsx";
// @ts-ignore
import useGoogleLogout from "react-google-login/src/use-google-logout";
import { useRecoilState } from "recoil";
import { authConfig } from "../../utils/auth.utils.js";
import { authState } from "../../atoms/auth.atoms.js";
import { useEffect, useState } from "react";
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";
import googleIcon from "../../assets/google-signin.png";
import useHistory from "../../hooks/useHistory.js";

export default function Header({ cb }) {
  const [auth, setAuth] = useRecoilState(authState);
  const [headerOpacity, setHeaderOpacity] = useState(0);
  const history = useHistory();
  const handleLogoutSuccess = () => {
    setAuth((prevState) => ({ ...prevState, isAuth: false }));
    window.location.reload();
  };

  const handleFailure = (resp) => {
    console.error(resp);
  };

  const { signOut } = useGoogleLogout({
    ...authConfig,
    onLogoutSuccess: handleLogoutSuccess,
    onFailure: handleFailure,
    onScriptLoadFailure: handleFailure,
  });

  const handleScroll = (scrollTop, scrollHeight) => {
    let opacity = scrollTop / scrollHeight / 0.15;
    if (opacity <= 1) {
      setHeaderOpacity(opacity);
    } else {
      setHeaderOpacity(1);
    }
  };

  useEffect(() => {
    cb(handleScroll);
  }, []);

  return (
    <Box h={pxToAll(80)} pos={"sticky"} top={"0"} zIndex={"3"} right={"0"}>
      <Box position="relative">
        <Box
          position="absolute"
          width="100%"
          height={pxToAll(80)}
          bg={"brand.primary"}
          opacity={headerOpacity}
        />
        <HStack
          justifyContent={"space-between"}
          alignItems={"center"}
          px={pxToAll(20)}
          py={pxToAll(20)}
        >
          <AgentDetect
            mobileComponent={<Logo />}
            desktopComponent={
              <HStack width={["100%", null, "40%"]} zIndex={"1"}>
                <Circle
                  size={pxToAll(35)}
                  bg="shade.secondary"
                  onClick={history.onPrev}
                >
                  <Icon
                    as={MdKeyboardArrowLeft}
                    textStyle={"icon.md"}
                    layerStyle={
                      !history.navState.isPrev ? "iconDisabled" : "hover"
                    }
                  />
                </Circle>
                <Circle
                  size={pxToAll(35)}
                  bg="shade.secondary"
                  onClick={history.onNext}
                >
                  <Icon
                    as={MdKeyboardArrowRight}
                    textStyle={"icon.md"}
                    layerStyle={
                      !history.navState.isNext ? "iconDisabled" : "hover"
                    }
                  />
                </Circle>
                <Routes>
                  <Route
                    path={`${ROUTER.SEARCH}/*`}
                    element={<SearchInput width={"100%"} />}
                  />
                </Routes>
              </HStack>
            }
          />
          <Box>
            {
              auth.isAuth ?
                <Button onClick={signOut}>Exit Musix</Button>
                :
                <Popover placement="bottom-end">
                  <PopoverTrigger>
                    <Button>Enter Musix</Button>
                  </PopoverTrigger>
                  <PopoverContent w={'max-content'} _focus={{ boxShadow: 'none' }}>
                    <PopoverArrow />
                    <PopoverBody>
                      <Image src={googleIcon} alt="Sign in with Google" height={pxToAll(40)} cursor="pointer" _hover={{ transform: 'scale(1.1)', transition: 'transform 0.25s' }} _active={{ transform: 'scale(1)', transition: 'transform 0.25s' }} onClick={() => {
                        if (auth.signIn !== undefined)
                          auth.signIn()
                      }} />
                    </PopoverBody>
                  </PopoverContent>
                </Popover>
            }
          </Box>
        </HStack>
      </Box>
    </Box>
  );
}
