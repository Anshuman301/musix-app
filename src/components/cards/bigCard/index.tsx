import { Box, IconButton, Image, Text } from "@chakra-ui/react";
import { useLayoutEffect, useRef, useState } from "react";
import { pxToAll, showOut } from "../../../utils/theme.utils.js";
import { FaPlay, FaPause } from "react-icons/fa";
import { useRecoilValue } from "recoil";
import { authState } from "../../../atoms/auth.atoms.js";
import useCustomToast from "../../../hooks/useCustomToast.js";
import fallbackRef from "../../../assets/logo-big.png";
import { debounce, handleImgResize } from "../../../utils/conversion.utils.js";
export default function BigCard({
  imageSource,
  imageBorderRadius,
  title,
  subtitle,
  onClick,
  otherProps,
}) {
  const {
    imageWidth,
    onPlayClick,
    onPauseClick,
    isPlaying,
    isLoading,
    cardHeight,
  } = otherProps;
  const [PlayButtonVisble, setPlayButtonVisble] = useState(false);
  const auth = useRecoilValue(authState);
  const toast = useCustomToast();
  const imgRef = useRef<HTMLImageElement>(null);
  const handlePlayClick = (e) => {
    e.stopPropagation();
    if (auth.isAuth) {
      if (!isLoading) {
        if (!isPlaying) onPlayClick();
        else onPauseClick();
      }
    } else toast();
  };

  const debounceHandleImgResize = debounce(handleImgResize, 500);
  function handleLoadImage(this: HTMLImageElement) {
    this.style.transition = `opacity 0.5s`;
    this.style.opacity = "1";
    debounceHandleImgResize(this);
    this.addEventListener("resize", () => {
      debounceHandleImgResize(this);
    });
  }
  useLayoutEffect(() => {
    if (imgRef.current !== null) {
      imgRef.current.addEventListener("load", handleLoadImage);
      return () => {
        imgRef.current?.removeEventListener("load", handleLoadImage);
      };
    }
  }, [imageWidth, imgRef]);
  return (
    <Box
      role="group"
      bgColor="blackAlpha.200"
      padding={"10px"}
      height={cardHeight}
      _hover={{ bgColor: "whiteAlpha.100" }}
      onMouseEnter={() => setPlayButtonVisble(true)}
      onMouseLeave={() => setPlayButtonVisble(false)}
      onClick={onClick}
      pos={"relative"}
    >
      <Box width={imageWidth} pos="relative">
        <Image
          borderRadius={imageBorderRadius}
          boxShadow="0 8px 24px rgb(0 0 0 / 50%)"
          src={imageSource}
          height={imageWidth}
          width={"100%"}
          objectFit="cover"
          fallbackSrc={fallbackRef}
          alt={`${title} - Image`}
          ref={imgRef}
          opacity={0.5}
        />
        {imageWidth === "100%" && (
          <PlayPauseIcon
            bottom={"5"}
            right={"5"}
            PlayButtonVisble={PlayButtonVisble}
            handlePlayClick={handlePlayClick}
            isLoading={isLoading}
            isPlaying={isPlaying}
          />
        )}
      </Box>
      {imageWidth !== "100%" && (
        <PlayPauseIcon
          bottom={"10"}
          right={"10"}
          PlayButtonVisble={PlayButtonVisble}
          handlePlayClick={handlePlayClick}
          isLoading={isLoading}
          isPlaying={isPlaying}
        />
      )}
      <Box paddingTop={pxToAll(15)}>
        <Text
          isTruncated
          textStyle={imageWidth !== "100%" ? "h4" : "h6"}
          color={"text.secondary"}
          fontWeight="bold"
        >
          {title}
        </Text>
        <Text noOfLines={2} textStyle={"label"}>
          {subtitle}
        </Text>
      </Box>
    </Box>
  );
}

BigCard.defaultProps = {
  imageBorderRadius: "4",
  cardHeight: "100%",
  imageWidth: "100%",
};

function PlayPauseIcon({
  right,
  bottom,
  PlayButtonVisble,
  isLoading,
  isPlaying,
  handlePlayClick,
}) {
  return (
    <IconButton
      aria-label="play-pause-butt-v2"
      pos="absolute"
      textAlign="right"
      right={`${right}px`}
      visibility={PlayButtonVisble ? "visible" : "hidden"}
      bottom={"0"}
      size="lg"
      _groupHover={{
        bottom: `${bottom}px`,
        transition: "bottom 1s visibility 1s",
      }}
      isLoading={isLoading}
      icon={isPlaying ? <FaPause /> : <FaPlay />}
      onClick={handlePlayClick}
    />
  );
}
