import { Image } from "@chakra-ui/react";
import { pxToAll, showOut } from "../../utils/theme.utils.js";
import logo from "../../assets/logo.png";

export default function Logo() {
  return (
    <Image
      src={logo}
      alt={"Musix"}
      height={pxToAll(40)}
      maxWidth={pxToAll(131)}
      width={"100%"}
      animation={`${showOut} 0.5s linear 1 normal`}
    />
  );
}
