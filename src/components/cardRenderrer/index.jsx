import { Grid } from "@chakra-ui/react";
import AgentDetect from "../util/AgentDetect";

function CardRenderer({ children, ...otherProps }) {
  const {
    minCardWidth,
    noOfRows,
    autoRows,
    noOfChildren,
    rowGap,
    columnGap,
    ...rest
  } = otherProps;
  return (
    <AgentDetect
      desktopComponent={
        <Grid
          templateColumns={`repeat(${noOfChildren},minmax(${minCardWidth}px, 1fr))`}
          templateRows={`repeat(${noOfRows},1fr)`}
          autoRows={otherProps.overflowY != "hidden" ? undefined : autoRows}
          rowGap={rowGap}
          columnGap={columnGap}
          {...rest}
        >
          {children}
        </Grid>
      }
      mobileComponent={
        <Grid
          templateColumns={`repeat(${noOfChildren},minmax(${minCardWidth}px, 1fr))`}
          templateRows={`repeat(${noOfRows},1fr)`}
          autoRows={otherProps.overflowY != "hidden" ? undefined : autoRows}
          rowGap={rowGap}
          columnGap={columnGap}
          css={{
            "&::-webkit-scrollbar": {
              display: "none",
            },
          }}
          {...rest}
        >
          {children}
        </Grid>
      }
    />
  );
}

CardRenderer.defaultProps = {
  minCardWidth: "165",
  noOfRows: 1,
  rowGap: "0",
  columnGap: "0.7rem",
  overflowX: "auto",
  overflowY: "hidden",
  autoRows: 0,
  noOfChildren: 5,
};

export default CardRenderer;
