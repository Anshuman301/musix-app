import { Box, Flex, HStack, Text } from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";
import useAgent from "../../hooks/useAgent.js";
import usePlayPauseClick from "../../hooks/usePlayPauseClick.js";
import ROUTER from "../../utils/constants/router.constants.js";
import { pxToAll } from "../../utils/theme.utils.js";
import CardRenderer from "../cardRenderrer/index.jsx";
import BigCard from "../cards/bigCard/index.js";
import CustomItem from "../util/CustomItem.jsx";
import type { ArtistType } from "../../atoms/albumPlayList.atom";
import React from "react";

export default function ContentWrapper(props) {
  const { as, property, autoRows, seeAll, noOfChildren, rowGap } = props;
  const items = props?.playlists?.items || props?.albums?.items || props?.rows;
  const title = props?.message?.name || props?.message || "";
  const navigate = useNavigate();
  const isMobile = useAgent();
  const showAllContent = () => {
    if (as === "recommend") navigate(`${as}/${property}`);
    else navigate(`${ROUTER.GENRE}/${property}`);
  };

  return (
    <Flex direction={"column"} py={pxToAll(10)}>
      <HStack justifyContent={"space-between"} mb={pxToAll(15)}>
        <Box w={"70%"}>
          <Text textStyle={"h5"} color={"text.secondary"} isTruncated>
            {title}
          </Text>
        </Box>
        {seeAll && (
          <CustomItem
            variant="card"
            onClick={() => showAllContent()}
            cursor={isMobile ? "auto" : "pointer"}
          >
            <Text fontWeight={"bold"} textStyle={"label"}>
              SEE ALL
            </Text>
          </CustomItem>
        )}
      </HStack>
      <CardRenderer
        autoRows={autoRows}
        noOfChildren={noOfChildren}
        rowGap={rowGap}
        minCardWidth={props.minCardWidth}
      >
        {items
          ?.filter((item) => item !== null)
          .map(({ ...rest }) => {
            const modId = rest?.id || rest?.spotifyId;
            return <BigCardWrapper {...rest} key={modId} id={modId} />;
          })}
      </CardRenderer>
    </Flex>
  );
}

export function BigCardWrapper(props) {
  const navigate = useNavigate();
  const title = props?.name ?? "";
  const subtitle = props?.description || props?.artists || "";
  const imageSource =
    props?.imgUrl || props?.images?.[0]?.url || props?.album?.images?.[0]?.url;
  const type = props?.album?.type || props?.type;
  const id = props?.spotifyId || props?.album?.id || props?.id;
  const { isPlaying, isLoading, handlePlayClick, handlePauseClick } =
    usePlayPauseClick(id);
  const handleClick = () => {
    navigate(`/${type}/${id}`);
  };
  const getSubtitle = (type: string) => {
    switch (type) {
    case ROUTER.ARTIST: {
      return "";
    }
    case ROUTER.UPLAYLIST: {
      const artistArr = props.artistIds?.split(",") as string[];
      if (!artistArr) return subtitle;
      const artistNameArr = subtitle?.replace(" and more", "")?.split(",");
      const arrEle = getArtistLabel(
        artistArr.map((id, idx) => ({ id, name: artistNameArr[idx] }))
      );
      arrEle.push(
        <React.Fragment
          key={`${title}-subtitle-more`}
        >{" and more"}</React.Fragment>
      );
      return arrEle;
    }
    default: {
      if (Array.isArray(subtitle)) {
        return getArtistLabel(subtitle);
      }
      return subtitle;
    }
    }
  };
  return (
    <BigCard
      imageBorderRadius={type === ROUTER.ARTIST ? "50%" : "10px"}
      imageSource={imageSource}
      title={title}
      subtitle={getSubtitle(type)}
      onClick={handleClick}
      otherProps={{
        imageWidth: props.width,
        cardHeight: props.cardHeight,
        onPlayClick: () =>
          handlePlayClick(type, title, imageSource, {
            artists: props.artists,
            description: props.description,
          }),
        onPauseClick: handlePauseClick,
        isPlaying,
        isLoading,
      }}
    />
  );
}
BigCardWrapper.defaultProps = {
  cardHeight: "100%",
  width: "100%",
};
ContentWrapper.defaultProps = {
  autoRows: 0,
  seeAll: true,
  noOfChildren: 5,
  rowGap: "0",
  minCardWidth: "165",
};

const ArtistLabel = ({ artist }: { artist: ArtistType }) => (
  <Link
    to={`/${ROUTER.ARTIST}/${artist.id}`}
    onClick={(e) => e.stopPropagation()}
  >
    {artist.name}
  </Link>
);

export const getArtistLabel = (artists: ArtistType[]) =>
  artists.reduce((acc: any[], artist: ArtistType, idx) => {
    if (idx !== 0) acc.push(<>, </>);
    acc.push(<ArtistLabel artist={artist} key={artist.id} />);
    return acc;
  }, [] as any[]);
