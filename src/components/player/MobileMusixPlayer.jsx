import {
  Box,
  Flex,
  HStack,
  Icon,
  Slide,
  useMediaQuery,
} from "@chakra-ui/react";
import { pxToAll, pxToRem, pxToRemSm } from "../../utils/theme.utils.js";
import { TrackLabel } from "./MusixPlayer";
import { AiOutlineDown } from "react-icons/ai";
import { FaPause, FaPlay } from "react-icons/fa";
import { CgPlayTrackNext } from "react-icons/cg";
import CustomSlider from "./CustomSlider.jsx";
import { ViewPlayItems } from "./DesktopMusixPlayer.jsx";
import { atom, useRecoilState } from "recoil";
import { Fragment } from "react";

export const openFullPlayerState = atom({
  key: "openFullPlayerState",
  default: false,
});

export default function MobileMusixPlayer({
  currentItem,
  handlePlayPauseClick,
  isPlaying,
  handleNextTrackClick,
  handlePlayerChange,
  handlePrevTrackClick,
  handleRepeatClick,
  handlePlayerChangeEnd,
  handleShuffleTrack,
  PlayerState,
  isDisabled
}) {
  const [isLarger] = useMediaQuery("(min-width:768px)");
  const [isOpenFullPlayer, setOpenFullPlayer] =
    useRecoilState(openFullPlayerState);
  return (
    <Fragment>
      <Slide
        direction="bottom"
        in={isOpenFullPlayer}
        style={{
          zIndex: 3,
          touchAction: "none",
          msTouchAction: "-ms-none",
        }}
        onPanEnd={(evt, info) => {
          const offsetX = info.offset.x,
            offsetY = info.offset.y;
          const slope = Math.abs(offsetY / offsetX);
          console.log("X:", offsetX, "Y:", offsetY, "Slope", slope);
          if (slope > 0.2 && slope <= 1) return;
          else if (slope > 0 && slope <= 0.2) {
            if (offsetX < 0) handleNextTrackClick(evt);
            else handlePrevTrackClick(evt);
          } else if (slope >= 2) {
            if (offsetY > 0) setOpenFullPlayer(false);
          }
        }}
      >
        <Box
          bgImage={currentItem?.imageSource}
          bgSize={"cover"}
          bgPos={["center"]}
          height={"100vh"}
          sx={{ filter: "blur(5px) grayscale(1) brightness(0.5)" }}
        />
        <Flex
          direction={"column"}
          px={pxToAll(20)}
          py={pxToAll(20)}
          pos={"absolute"}
          w={"100%"}
          top={"0"}
          height={"100%"}
        >
          <HStack justifyContent={"space-between"} mb={pxToAll(20)}>
            <Icon
              as={AiOutlineDown}
              textStyle={"icon.md"}
              onClick={() => setOpenFullPlayer(false)}
            />
            <ViewPlayItems />
          </HStack>
          <Box pos={"relative"} top={"60%"} rowGap={pxToAll(20)}>
            {!!currentItem && (
              <TrackLabel
                currentItem={currentItem}
                imageSize={150}
                maxWidth={175}
              />
            )}
            <Box mb={pxToAll(20)} />
            <CustomSlider
              handleNextTrackClick={handleNextTrackClick}
              handlePlayPauseClick={handlePlayPauseClick}
              handlePlayerChange={handlePlayerChange}
              handlePlayerChangeEnd={handlePlayerChangeEnd}
              handlePrevTrackClick={handlePrevTrackClick}
              handleRepeatClick={handleRepeatClick}
              isPlaying={isPlaying}
              isDisabled={isDisabled}
              PlayerState={PlayerState}
              handleShuffleTrack={handleShuffleTrack}
            />
          </Box>
        </Flex>
      </Slide>
      <Slide
        direction="bottom"
        in={isPlaying !== false}
        style={{
          zIndex: 1,
          bottom: isLarger ? pxToRem(75) : pxToRemSm(75 / 1.5),
        }}
      >
        <Flex
          direction={"row"}
          alignItems={"center"}
          pr={pxToAll(20)}
          bg={"brand.secondary"}
          w={"100%"}
          height={pxToAll(100)}
          boxShadow={`0 -5px 25px rgba(0,0,0,0.2)`}
          onClick={() => setOpenFullPlayer(true)}
        >
          <Box width={"80%"} height={"100%"}>
            {!!currentItem && (
              <TrackLabel
                currentItem={currentItem}
                imageSize={100}
                maxWidth={150}
              />
            )}
          </Box>
          <Flex w={"20%"} columnGap={pxToAll(20)} justify={"flex-end"}>
            <Icon
              onClick={handlePlayPauseClick}
              textStyle={"icon.md"}
              as={isPlaying ? FaPause : FaPlay}
              layerStyle="hover"
            />
            <Icon
              textStyle={"icon.md"}
              as={CgPlayTrackNext}
              onClick={handleNextTrackClick}
              layerStyle="hover"
            />
          </Flex>
        </Flex>
      </Slide>
    </Fragment>
  );
}
