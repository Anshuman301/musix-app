import CustomSlider from "./CustomSlider";
import { pxToAll } from "../../utils/theme.utils.js";
import { TrackLabel } from "./MusixPlayer";
import {
  Flex,
  Grid,
  GridItem,
  Icon,
  Slider,
  SliderFilledTrack,
  SliderTrack,
} from "@chakra-ui/react";
import { GiSoundOn, GiSoundOff } from "react-icons/gi";
import { RiPlayList2Fill } from "react-icons/ri";
import { useLocation, useNavigate } from "react-router-dom";
import ROUTER from "../../utils/constants/router.constants.js";
export default function DesktopMusixPlayer({
  PlayerState,
  currentItem,
  handlePlayerChange,
  isPlaying,
  handlePlayerChangeEnd,
  handlePlayPauseClick,
  handleNextTrackClick,
  handleRepeatClick,
  handlePrevTrackClick,
  handleVolChange,
  handleShuffleTrack,
  isDisabled,
}: any) {
  return (
    <Grid
      gridTemplateColumns={"minmax(200px, 1fr) minmax(300px, 1fr) 1fr"}
      columnGap={"30px"}
      px={pxToAll(20)}
      bg={"brand.secondary"}
      w={"100%"}
      height={pxToAll(120)}
      pos={"fixed"}
      bottom={"0"}
      zIndex={"1"}
      boxShadow={"0 -5px 25px rgba(0,0,0,0.2)"}
      alignItems={"center"}
    >
      <GridItem>
        {!!currentItem && (
          <TrackLabel currentItem={currentItem} imageSize={75} maxWidth={175} />
        )}
      </GridItem>
      <GridItem>
        <CustomSlider
          PlayerState={PlayerState}
          handlePlayerChange={handlePlayerChange}
          isDisabled={isDisabled}
          handlePlayerChangeEnd={handlePlayerChangeEnd}
          handlePrevTrackClick={handlePrevTrackClick}
          handlePlayPauseClick={handlePlayPauseClick}
          isPlaying={isPlaying}
          handleNextTrackClick={handleNextTrackClick}
          handleRepeatClick={handleRepeatClick}
          handleShuffleTrack={handleShuffleTrack}
        />
      </GridItem>
      <GridItem>
        <Flex
          direction={"row"}
          justifyContent={"flex-end"}
          columnGap={pxToAll(20)}
          alignItems={"center"}
        >
          <ViewPlayItems />
          <Icon
            as={PlayerState.volume ? GiSoundOn : GiSoundOff}
            textStyle={"icon.md"}
            layerStyle={"hover"}
            onClick={() => handleVolChange(PlayerState.volume ? 0 : 10)}
          />
          <Slider
            aria-label="slider-ex-2"
            defaultValue={5}
            w={pxToAll(100)}
            isDisabled={isDisabled}
            max={10}
            onChange={handleVolChange}
            value={PlayerState.volume}
          >
            <SliderTrack bg={"shade.hoverPrimary"}>
              <SliderFilledTrack bg={"text.play"} />
            </SliderTrack>
          </Slider>
        </Flex>
      </GridItem>
    </Grid>
  );
}

export function ViewPlayItems() {
  const navigate = useNavigate();
  const path = useLocation().pathname as string;
  const handleClick = () => {
    if (path !== `/${ROUTER.QUEUE}`) navigate(ROUTER.QUEUE);
    else history.back();
  };
  return (
    <Icon
      as={RiPlayList2Fill}
      textStyle={"icon.md"}
      color={path === `/${ROUTER.QUEUE}` ? "text.play" : "text.primary"}
      onClick={handleClick}
      layerStyle={"hover"}
    />
  );
}
