import { secondsToMins } from "../../utils/conversion.utils.js";
import { pxToAll } from "../../utils/theme.utils.js";
import {
  Box,
  Flex,
  Icon,
  IconButton,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Text,
} from "@chakra-ui/react";
import { FaPause, FaPlay } from "react-icons/fa";
import { GiSoundWaves } from "react-icons/gi";
import { CgPlayTrackNext, CgPlayTrackPrev } from "react-icons/cg";
import { BiShuffle } from "react-icons/bi";
import { MdRepeat, MdRepeatOn, MdRepeatOneOn, MdShuffleOn } from "react-icons/md";
import {
  PLAYMODE,
  REPMODE,
} from "../../utils/constants/trackState.constants.js";
import { Fragment } from "react";
import useAgent from "../../hooks/useAgent.js";
export default function CustomSlider({
  handlePlayerChange,
  isDisabled,
  handlePlayerChangeEnd,
  handlePrevTrackClick,
  handlePlayPauseClick,
  isPlaying,
  handleNextTrackClick,
  handleRepeatClick,
  handleShuffleTrack,
  PlayerState,
}) {
  const isMobile = useAgent();
  const { isShuffle, isRepeat } = PlayerState
  const repComp = {
    [REPMODE.ALL]: MdRepeatOn,
    [REPMODE.NONE]: MdRepeat,
    [REPMODE.ONCE]: MdRepeatOneOn,
  };
  return (
    <Fragment>
      {" "}
      <Flex direction={"row"} wrap="nowrap">
        <Text textStyle={"label"}>
          {secondsToMins(PlayerState.currentTime)}
        </Text>
        <Slider
          aria-label="slider-ex-2"
          defaultValue={0}
          value={PlayerState.currentTime}
          onChange={handlePlayerChange}
          max={PlayerState.totalDuration}
          isDisabled={isDisabled}
          onChangeEnd={handlePlayerChangeEnd}
          focusThumbOnChange={false}
          mx={pxToAll(15)}
        >
          <SliderTrack bg={"shade.hoverPrimary"}>
            <SliderFilledTrack bg={"text.play"} />
          </SliderTrack>
          <SliderThumb bg={"text.primary"} hidden>
            <Box color={"text.play"} as={GiSoundWaves} />
          </SliderThumb>
        </Slider>
        <Text textStyle={"label"}>
          {secondsToMins(PlayerState.totalDuration)}
        </Text>
      </Flex>
      <Flex
        direction={"row"}
        w="100%"
        justifyContent={"center"}
        wrap="nowrap"
        columnGap={pxToAll(20)}
        marginTop={pxToAll(5)}
        alignItems={"center"}
      >
        <Icon
          as={isShuffle ? MdShuffleOn : BiShuffle}
          textStyle={isMobile ? "icon.md" : "icon.sm"}
          layerStyle={"hover"}
          onClick={handleShuffleTrack}
        />
        <IconButton
          aria-label="prev-track-butt"
          size={isMobile ? "lg" : "mdlg"}
          icon={<CgPlayTrackPrev />}
          isDisabled={isDisabled}
          onClick={handlePrevTrackClick}
          layerStyle={"hover"}
        />
        <IconButton
          aria-label="play-pause-track-butt"
          onClick={handlePlayPauseClick}
          isLoading={isPlaying === PLAYMODE.LOADING}
          size={isMobile ? "xl" : "mdlg"}
          isDisabled={isDisabled}
          icon={isPlaying ? <FaPause /> : <FaPlay />}
          layerStyle={"hover"}
        />
        <IconButton
          aria-label="next-track-butt"
          size={isMobile ? "lg" : "mdlg"}
          icon={<CgPlayTrackNext />}
          isDisabled={isDisabled}
          onClick={handleNextTrackClick}
          layerStyle={"hover"}
        />
        <Icon
          as={repComp[isRepeat]}
          textStyle={isMobile ? "icon.md" : "icon.sm"}
          onClick={handleRepeatClick}
          layerStyle={"hover"}
        />
      </Flex>
    </Fragment>
  );
}
