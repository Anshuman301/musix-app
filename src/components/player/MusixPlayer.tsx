import { Box, HStack, Icon, Image, Text } from "@chakra-ui/react";
import { keyframes } from "@emotion/react";
import useAgent from "../../hooks/useAgent.js";
import { pxToAll, showOut } from "../../utils/theme.utils.js";
import { FcLike } from "react-icons/fc";
import { GiSelfLove } from "react-icons/gi";
import {
  selector,
  useRecoilState,
  useRecoilValue,
  useRecoilValueLoadable,
} from "recoil";
import { musixToken, youtubeSearch } from "../../utils/auth.utils.js";
import { musixAxios } from "../../utils/axios.utils.js";
import {
  PLAYMODE,
  REPMODE,
} from "../../utils/constants/trackState.constants.js";
import ROUTER from "../../utils/constants/router.constants.js";
import { authState } from "../../atoms/auth.atoms.js";
import { getNewStateForPlayPause } from "../../utils/conversion.utils.js";
import { albumPlayListSelectorTrackState } from "../../selector/albumPlayList.selector.js";
import {
  albumPlayListTrackState,
  ItemType,
  likedItemsState,
  searchTrackState,
} from "../../atoms/albumPlayList.atom.js";
import useLiked from "../../hooks/useLiked.js";
import AgentDetect from "../util/AgentDetect.jsx";
import MobileMusixPlayer from "./MobileMusixPlayer.jsx";
import DesktopMusixPlayer from "./DesktopMusixPlayer.jsx";
import usePlayBack from "../../hooks/usePlayBack.js";
import { Fragment, useEffect, useLayoutEffect, useRef, useState } from "react";
import { getArtistLabel } from "../ContentWrapper/ContentWrapper.js";
import usePlayPauseClick from "../../hooks/usePlayPauseClick.js";

const init = {
  track: "",
  totalDuration: 0,
  currentTime: 0,
  isEnded: false as boolean | "PARTIAL_TRUE",
  isSliding: false,
  isRepeat: REPMODE.NONE,
  volume: 0,
  isShuffle: false,
};

const audioTrackState = selector({
  key: "audioTrackState",
  get: async ({ get }) => {
    const auth = get(authState);
    let search = get(searchTrackState);
    if (auth.isAuth && search) {
      search = encodeURIComponent(search);
      try {
        const resp = await musixAxios(musixToken()).get(
          `/youtubeSearch/${search}`
        );
        if (resp.status === 200) {
          return resp.data?.value;
        }
      } catch (e) {
        const [data, error] = await youtubeSearch(search);
        if (error) throw error;
        await musixAxios(musixToken()).post("/youtubeSearch/", {
          searchName: decodeURIComponent(search),
          ...data,
        });
        return data;
      }
    } else {
      return init;
    }
  },
});

export default function MusixPlayer() {
  const player = useRef<HTMLAudioElement>(null);
  const audioTrackLoadable = useRecoilValueLoadable(audioTrackState);
  const searchTrack = useRecoilValue(searchTrackState);
  const albumPlayListSelectorTrack = useRecoilValue(
    albumPlayListSelectorTrackState
  );
  const [albumPlayListTrack, setAlbumPlayListTrack] = useRecoilState(
    albumPlayListTrackState
  );
  const [handleNextTrack, handlePrevTrack, handleRepeat] = usePlayBack();
  const isPlaying = albumPlayListTrack?.isPlaying ?? false;
  const isDisabled = [0, 1].includes(Object.keys(albumPlayListTrack).length);
  const currentItem = albumPlayListSelectorTrack?.currentItem;
  const loadingState = audioTrackLoadable.state;
  const audioContent = audioTrackLoadable.contents;
  const [PlayerState, setPlayerState] = useState(() => {
    const isRepeat = window.localStorage.getItem("isRepeat") ?? REPMODE.NONE;
    const isShuffle = Boolean(Number(localStorage.getItem("isShuffle")));
    return { ...init, isRepeat, isShuffle, volume: 10 };
  });
  const { handleEmptyPlayTrack } = usePlayPauseClick(albumPlayListTrack.id);

  useEffect(() => {
    if (searchTrack) {
      if (loadingState === "hasValue")
        if (searchTrack)
          setPlayerState((prevState) => {
            const track = `${
              import.meta.env.SNOWPACK_PUBLIC_REACT_APP_YTB_STR_URL
            }/youtube.com/watch?v=${audioContent.videoId}`;
            return {
              ...PlayerState,
              track,
              totalDuration: audioContent.totalDuration,
              isEnded: prevState.track === track,
            };
          });
        else setPlayerState(init);
    }
    return () => {
      if (albumPlayListTrack.type === ROUTER.UPLAYLIST)
        localStorage.removeItem("albumPlayListTrackState");
      else
        localStorage.setItem(
          "albumPlayListTrackState",
          JSON.stringify({ ...albumPlayListTrack, isPlaying: PLAYMODE.PAUSE })
        );
    };
  }, [loadingState, searchTrack]);

  useEffect(() => {
    if (PlayerState.isEnded === true)
      setPlayerState({ ...PlayerState, isEnded: false });
  }, [PlayerState.isEnded]);
  // Adding onTimeUpdate Event listener to player
  useEffect(() => {
    if (player.current && PlayerState.track && PlayerState.isEnded === false) {
      player.current.src = PlayerState.track;
      player.current.onstalled = () => {
        setAlbumPlayListTrack((prevState) => ({
          ...prevState,
          isPlaying: PLAYMODE.LOADING,
        }));
      };
      player.current.onloadeddata = () => {
        if (player.current) {
          setAlbumPlayListTrack((prevState) => ({
            ...prevState,
            isPlaying: PLAYMODE.PLAYING,
          }));
          player.current.play();
          playItemsTrack();
        }
      };
      player.current.ontimeupdate = (event) => {
        if (player.current) {
          setPlayerState((prevState) => ({
            ...prevState,
            currentTime: prevState.isSliding
              ? prevState.currentTime
              : player.current?.currentTime ?? 0,
          }));
        }
      };
    }
  }, [PlayerState.track, PlayerState.isEnded]);

  useEffect(() => {
    if (player.current && PlayerState.track)
      player.current.onended = handleOnEndedPlayer;
  }, [PlayerState.track, PlayerState.isEnded, PlayerState.isRepeat]);

  const handlePlayerChange = (value: number) => {
    setPlayerState((previousState) => ({
      ...previousState,
      currentTime: value,
      isSliding: true,
    }));
  };

  const handlePlayerChangeEnd = (value: number) => {
    if (player.current) {
      setPlayerState((prevState) => ({ ...prevState, isSliding: false }));
      player.current.currentTime =
        value == PlayerState.totalDuration ? value - 1 : value;
    }
  };

  useEffect(() => {
    if (player.current)
      if (player.current.src === PlayerState.track) {
        if (isPlaying === PLAYMODE.PLAYING) player.current.play();
        else if (isPlaying === PLAYMODE.PAUSE) player.current.pause();
      }
  }, [player, isPlaying]);

  const handlePlayPauseClick = async (e) => {
    e.stopPropagation();
    const { nextIdx, totalLength } = albumPlayListSelectorTrack;
    let currentIdx = nextIdx - 1;
    if (nextIdx == 0) currentIdx = totalLength - 1;
    if (PlayerState.track) {
      if (isPlaying) {
        setAlbumPlayListTrack((prevState) =>
          getNewStateForPlayPause(prevState, PLAYMODE.PAUSE, currentIdx)
        );
      } else {
        if (PlayerState.isEnded === "PARTIAL_TRUE")
          setPlayerState((prevState) => ({ ...prevState, isEnded: true }));
        setAlbumPlayListTrack((prevState) =>
          getNewStateForPlayPause(prevState, PLAYMODE.PLAYING, currentIdx)
        );
      }
    } else {
      await handleEmptyPlayTrack();
    }
  };

  const handleRepeatTrack = (totalLength: number, nextIdx: number) => {
    if (totalLength == 1) {
      setPlayerState((prevState) => ({
        ...prevState,
        isEnded: true,
      }));
    } else if (nextIdx == 0) handleRepeat(PlayerState.isShuffle);
    else handleNextTrack(PlayerState.isShuffle);
  };
  const handleNonRepeatTrack = (
    totalLength: number,
    nextIdx: number,
    currentIdx: number
  ) => {
    if (!PlayerState.isShuffle && (totalLength == 1 || nextIdx == 0)) {
      setPlayerState((prevState) => ({
        ...prevState,
        isEnded: "PARTIAL_TRUE",
      }));
      setAlbumPlayListTrack((prevState) =>
        getNewStateForPlayPause(prevState, PLAYMODE.PAUSE, currentIdx)
      );
    } else handleNextTrack(PlayerState.isShuffle);
  };
  const handleOnEndedPlayer = (event) => {
    const {
      totalLength,
      nextIdx,
      currIdx: currentIdx,
    } = albumPlayListSelectorTrack;
    if (PlayerState.isRepeat == REPMODE.ALL)
      handleRepeatTrack(totalLength, nextIdx);
    else if (PlayerState.isRepeat == REPMODE.NONE)
      handleNonRepeatTrack(totalLength, nextIdx, currentIdx);
    else {
      setPlayerState((prevState) => ({
        ...prevState,
        isEnded: true,
      }));
    }
  };
  const handleNextTrackClick = (e) => {
    e.stopPropagation();
    const { totalLength, nextIdx } = albumPlayListSelectorTrack;
    handleRepeatTrack(totalLength, nextIdx);
  };

  const handlePrevTrackClick = () => {
    const { totalLength } = albumPlayListSelectorTrack;
    if (totalLength === 1)
      setPlayerState((prevState) => ({
        ...prevState,
        isEnded: true,
      }));
    else handlePrevTrack(PlayerState.isShuffle);
  };

  const handleRepeatClick = () => {
    let isRepeat = PlayerState.isRepeat;
    if (isRepeat === REPMODE.ONCE) isRepeat = REPMODE.NONE;
    else isRepeat = isRepeat + "0";
    setPlayerState({ ...PlayerState, isRepeat });
    window.localStorage.setItem("isRepeat", isRepeat);
  };

  useEffect(() => {
    if (player.current) player.current.volume = PlayerState.volume / 10;
  }, [PlayerState.volume]);

  const handleVolChange = (volume: number) => {
    setPlayerState({ ...PlayerState, volume });
  };

  const handleShuffleTrack = () => {
    setPlayerState((prevState) => {
      localStorage.setItem(
        "isShuffle",
        Number(!prevState.isShuffle).toString()
      );
      return { ...prevState, isShuffle: !prevState.isShuffle };
    });
  };

  async function playItemsTrack() {
    const { itemId, song, artists, imageSource } =
      albumPlayListSelectorTrack.currentItem;
    await musixAxios(musixToken()).put("/playItems/", {
      type: "track",
      spotifyId: itemId,
      name: song,
      description: "",
      imgUrl: imageSource,
      artists,
    });
  }
  return (
    <Fragment>
      <audio ref={player} />
      <AgentDetect
        mobileComponent={
          <MobileMusixPlayer
            PlayerState={PlayerState}
            currentItem={currentItem}
            isPlaying={isPlaying}
            handlePlayerChange={handlePlayerChange}
            handlePlayPauseClick={handlePlayPauseClick}
            handleNextTrackClick={handleNextTrackClick}
            handlePrevTrackClick={handlePrevTrackClick}
            handleRepeatClick={handleRepeatClick}
            handlePlayerChangeEnd={handlePlayerChangeEnd}
            handleShuffleTrack={handleShuffleTrack}
            isDisabled={isDisabled}
          />
        }
        desktopComponent={
          <DesktopMusixPlayer
            PlayerState={PlayerState}
            currentItem={currentItem}
            handlePlayerChange={handlePlayerChange}
            handleNextTrackClick={handleNextTrackClick}
            handlePrevTrackClick={handlePrevTrackClick}
            isPlaying={isPlaying}
            handlePlayerChangeEnd={handlePlayerChangeEnd}
            handlePlayPauseClick={handlePlayPauseClick}
            handleRepeatClick={handleRepeatClick}
            handleVolChange={handleVolChange}
            handleShuffleTrack={handleShuffleTrack}
            isDisabled={isDisabled}
          />
        }
      />
    </Fragment>
  );
}

interface TrackLabelProps {
  currentItem: ItemType;
  imageSize: number;
  maxWidth: number;
}

export function TrackLabel({
  currentItem,
  imageSize,
  maxWidth,
}: TrackLabelProps) {
  const { imageSource, song, itemId, artists } = currentItem;
  const songRef = useRef<HTMLParagraphElement>(null);
  const songContRef = useRef<HTMLDivElement>(null);
  const [animationReq, setAnimationReq] = useState(false);
  const onLiked = useLiked();
  const likedItems = useRecoilValue(likedItemsState);
  const isMobile = useAgent();
  const artistName = getArtistLabel(artists);
  const isLiked = !!likedItems[itemId] && likedItems[itemId] === "track";
  useLayoutEffect(() => {
    if (songRef.current && songContRef.current) {
      if (
        parseFloat(window.getComputedStyle(songRef.current).width) ==
        parseFloat(window.getComputedStyle(songContRef.current).maxWidth)
      )
        setAnimationReq(true);
      else setAnimationReq(false);
    }
  }, [songRef.current?.innerText]);

  const slide = keyframes`from {
    transform: translateX(-125px)
  }
  to {
    transform: translateX(125px)
  }
  `;
  return (
    <HStack columnGap={pxToAll(10)}>
      <Box maxWidth={pxToAll(imageSize)}>
        <Image
          src={imageSource}
          animation={`${showOut} 0.5s linear 1 normal`}
        />
      </Box>
      <Box maxW={`${maxWidth}px`} overflow={"hidden"} ref={songContRef}>
        <Text
          textStyle={"h6"}
          fontWeight={isMobile ? "bold" : "normal"}
          color={"text.play"}
          whiteSpace={"nowrap"}
          animation={
            animationReq
              ? `10s linear 0.25s infinite alternate ${slide}`
              : "none"
          }
          ref={songRef}
        >
          {song}
        </Text>
        <Text textStyle={"label"} isTruncated>
          {artistName}
        </Text>
      </Box>
      <Icon
        as={isLiked ? FcLike : GiSelfLove}
        textStyle={"icon.md"}
        _hover={{
          color: "text.play",
          transform: "scale(1.1)",
          transition: "all 0.25s",
        }}
        onClick={(e) => {
          e.stopPropagation();
          onLiked(itemId, "track");
        }}
      />
    </HStack>
  );
}
