import React from "react";
import { useLocation } from "react-router-dom";
export default function useHistory() {
  const location = useLocation();
  const handleNavState = () => {
    const state = history.state;
    const isNotNull = state != null;
    return {
      isPrev: isNotNull && state?.idx != 0,
      isNext: isNotNull && history.length - state?.idx !== 2,
    };
  };
  const navState = React.useMemo(handleNavState, [location.pathname]);

  const onNext = () => {
    if (navState.isNext) history.forward();
  };

  const onPrev = () => {
    if (navState.isPrev) history.back();
  };
  return { onNext, onPrev, navState };
}
