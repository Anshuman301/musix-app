import { snapshot_UNSTABLE, useRecoilCallback, useRecoilState, useRecoilValue } from "recoil";
import {
  albumPlayListTrackState,
  ModAlbumPlaylistSelType,
  searchTrackState,
} from "../atoms/albumPlayList.atom.js";
import {
  albumPlayListSelectorTrackState,
  albumPlayListDetailsSate,
} from "../selector/albumPlayList.selector.js";
import { musixToken } from "../utils/auth.utils.js";
import { musixAxios } from "../utils/axios.utils.js";
import { PLAYMODE } from "../utils/constants/trackState.constants.js";
import {
  getNewStateForPlayPause,
  searchTrackTemplate,
} from "../utils/conversion.utils.js";

export default function usePlayPauseClick(id: string) {
  const [albumPlayListTrack, setAlbumPlayListTrack] = useRecoilState(
    albumPlayListTrackState
  );
  const [albumPlayListSelectorTrack, setAlbumPlayListSelTrack] = useRecoilState(
    albumPlayListSelectorTrackState
  );
  const searchTrack = useRecoilValue(searchTrackState)
  const isPlaying =
    albumPlayListTrack?.id === id &&
    albumPlayListTrack?.isPlaying === PLAYMODE.PLAYING;

  const isLoading =
    albumPlayListTrack?.id === id &&
    albumPlayListTrack?.isPlaying === PLAYMODE.LOADING;

  const handlePlayCallback = useRecoilCallback(
    ({ snapshot, set }) =>
      async (type: string, id: string) => {
        try {
          const albumPlayListDetails = await snapshot.getPromise(
            albumPlayListDetailsSate({
              type,
              id,
            })
          );
          const items =
            albumPlayListDetails?.tracks?.items ||
            albumPlayListDetails?.tracks ||
            [];
          const searchTrack = searchTrackTemplate(
            items[0]?.track?.name || items[0].name,
            items[0]?.track?.artists[0].name || items[0]?.artists[0].name
          );
          const itemId: string = items[0]?.track?.id || items[0]?.id;
          set(albumPlayListSelectorTrackState, {
            id: itemId,
            searchTrack,
            type,
            albumPlayListId: id,
          } as ModAlbumPlaylistSelType);
        } catch (e) {
          console.error(e);
        }
      },
    []
  );
  const handlePlayClick = async (
    type: string,
    title: string,
    imageSource: string,
    props
  ) => {
    if (albumPlayListTrack?.id === id) {
      if(searchTrack) {
        const { nextIdx, totalLength } = albumPlayListSelectorTrack;
        let currentIdx = nextIdx - 1;
        if (nextIdx == 0) currentIdx = totalLength - 1;
        setAlbumPlayListTrack((prevState) =>
          getNewStateForPlayPause(prevState, PLAYMODE.PLAYING, currentIdx)
        );
      } else {
        await handleEmptyPlayTrack()
      }
    } else {
      const resp = await musixAxios(musixToken()).put("/playItems/", {
        type,
        spotifyId: id,
        name: title,
        description: props.description,
        imgUrl: imageSource,
        artists: props.artists,
      });
      if (!(resp.status >= 400)) await handlePlayCallback(type, id);
    }
  };

  const handlePauseClick = () => {
    const { nextIdx, totalLength } = albumPlayListSelectorTrack;
    let currentIdx = nextIdx - 1;
    if (nextIdx == 0) currentIdx = totalLength - 1;
    setAlbumPlayListTrack((prevState) =>
      getNewStateForPlayPause(prevState, PLAYMODE.PAUSE, currentIdx)
    );
  };

  const handleEmptyPlayTrack = async () => {
    const item = albumPlayListTrack.items.find(item => item.isPlaying === PLAYMODE.PLAYING || item.isPlaying === PLAYMODE.PAUSE)
      if (item) {
        const { artists, itemId, song } = item
        const snapshot = snapshot_UNSTABLE()
        const release = snapshot.retain();
        try {
          const { id, type } = albumPlayListTrack
          await snapshot.getPromise(albumPlayListDetailsSate({ type, id }));
          setAlbumPlayListSelTrack({
            id: itemId,
            searchTrack: searchTrackTemplate(song, artists[0].name),
            type,
            albumPlayListId: albumPlayListTrack.id,
          } as ModAlbumPlaylistSelType);
        } finally {
          release()
        }
      }
  }

  return { isPlaying, isLoading, handlePlayClick, handlePauseClick, handleEmptyPlayTrack };
}
