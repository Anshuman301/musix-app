import React from "react";
import { spotifyAuth } from "../utils/spotify.utils";
export default function useNetworkAgent() {
  const [status, setStatus] = React.useState<{ isOnline: boolean | "" }>({
    isOnline: "",
  });
  React.useEffect(() => {
    window.addEventListener("online", handleNetwork);
    window.addEventListener("offline", handleNetwork);
    return () => {
      window.removeEventListener("online", handleNetwork);
      window.removeEventListener("offline", handleNetwork);
    };
  }, []);

  async function handleNetwork() {
    if (navigator.onLine) {
      await spotifyAuth(true);
      setStatus((prev) => ({ ...prev, isOnline: true }));
      setTimeout(() => {
        setStatus((prev) => ({ ...prev, isOnline: "" }));
      }, 2000);
    } else {
      setStatus((prev) => ({ ...prev, isOnline: false }));
    }
  }
  return status;
}
