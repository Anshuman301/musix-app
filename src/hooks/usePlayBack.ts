import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import {
  albumPlayListTrackState,
  albumPlayListTrackType,
  searchTrackState,
} from "../atoms/albumPlayList.atom.js";
import { albumPlayListSelectorTrackState } from "../selector/albumPlayList.selector.js";
import { PLAYMODE } from "../utils/constants/trackState.constants.js";
import { getNewStateForRepeatPlayBack, searchTrackTemplate } from "../utils/conversion.utils.js";

export default function usePlayBack() {
  const albumPlayListSelectorTrack = useRecoilValue(
    albumPlayListSelectorTrackState
  );
  const { nextIdx, prevIdx, totalLength, nextSearchTrack, prevSearchTrack, currIdx } =
    albumPlayListSelectorTrack;
  const setSearchTrack = useSetRecoilState(searchTrackState);
  const [albumPlayListTrack, setAlbumPlayListTrack] = useRecoilState(albumPlayListTrackState);

  const handleNextTrack = (isShuffle: boolean) => {
    if(isShuffle)
    return handleShuffle();
    setSearchTrack(nextSearchTrack);
    setAlbumPlayListTrack((prevState) => ({
      ...prevState,
      items: [
        ...prevState.items.slice(0, nextIdx - 1),
        {
          ...prevState.items[nextIdx - 1],
          isPlaying: PLAYMODE.INQUEUE,
        },
        {
          ...prevState.items[nextIdx],
          isPlaying: PLAYMODE.PLAYING,
        },
        ...prevState.items.slice(nextIdx + 1),
      ],
    }));
  };

  const handlePrevTrack = (isShuffle: boolean) => {
    if(isShuffle)
    return handleShuffle()
    setSearchTrack(prevSearchTrack);
    setAlbumPlayListTrack((prevState) => {
      if (totalLength - 1 === prevIdx)
        return getNewStateForRepeatPlayBack(
          prevState,
          totalLength,
          PLAYMODE.INQUEUE,
          PLAYMODE.PLAYING
        );
      else
        return {
          ...prevState,
          items: [
            ...prevState.items.slice(0, prevIdx),
            {
              ...prevState.items[prevIdx],
              isPlaying: PLAYMODE.PLAYING,
            },
            {
              ...prevState.items[prevIdx + 1],
              isPlaying: PLAYMODE.INQUEUE,
            },
            ...prevState.items.slice(prevIdx + 2),
          ],
        };
    });
  };

  const handleShuffle = () => {
    const items = albumPlayListTrack.items;
    const randIdx = Math.floor(Math.random() * items.length)
    const selIdx = randIdx === currIdx ? nextIdx : randIdx
    const searchTrack = searchTrackTemplate(items[randIdx].song, items[randIdx].artists[0].name)
    setSearchTrack(searchTrack);
    setAlbumPlayListTrack(prevState => {
      const state = JSON.parse(JSON.stringify(prevState)) as albumPlayListTrackType
      state.items[currIdx].isPlaying = PLAYMODE.INQUEUE
      state.items[selIdx].isPlaying = PLAYMODE.PLAYING
      return state;
    })
  };

  const handleRepeat = (isShuffle: boolean) => {
    if(isShuffle)
    return handleShuffle();
    setSearchTrack(nextSearchTrack);
    setAlbumPlayListTrack((prevState) =>
      getNewStateForRepeatPlayBack(
        prevState,
        totalLength,
        PLAYMODE.PLAYING,
        PLAYMODE.INQUEUE
      )
    );
  };

  return [handleNextTrack, handlePrevTrack, handleRepeat];
}
