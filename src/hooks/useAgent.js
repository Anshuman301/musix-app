import React from "react";
import { atom, useRecoilValue } from "recoil";

export const uAgentState = atom({
  key: "uAgentState",
  default: false,
});
export default function useAgent() {
  const uAgent = useRecoilValue(uAgentState);
  return React.useMemo(() => uAgent, []);
}
