import { Box, Flex, Spinner, Text } from "@chakra-ui/react";
import { useEffect, useRef } from "react";
import { Link, Outlet } from "react-router-dom";
import { useRecoilCallback, useRecoilValueLoadable } from "recoil";
import Header from "../components/header/Header";
import MusixPlayer from "../components/player/MusixPlayer";
import DesktopSideBar from "../components/sidebar/DesktopSideBar";
import MobileSideBar from "../components/sidebar/MobileSideBar";
import AgentDetect from "../components/util/AgentDetect";
import CustomSuspense from "../components/util/CustomSuspense";
import useNetworkAgent from "../hooks/useNetworkAgent";
import { spotifyAuthState } from "../selector/auth.selector.js";
import { spotifyAuth } from "../utils/spotify.utils.js";
import { pxToAll, pxToRem } from "../utils/theme.utils.js";
let setHeader = (...params) => console.debug(params);
const headerCallBack = (func) => (setHeader = func);
export default function HomePage() {
  const ref = useRef<HTMLDivElement>(null);
  const spotifyAuthLodableState =
    useRecoilValueLoadable(spotifyAuthState).state;

  const spotifyAuthCallback = useRecoilCallback(
    () => async () => {
      await spotifyAuth();
    },
    []
  );

  useEffect(() => {
    const intervalId = setInterval(spotifyAuthCallback, 3599 * 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return (
    <Flex direction={"column"} wrap={"nowrap"}>
      <NetworkNotify />
      <Flex direction={"row"} wrap={"nowrap"}>
        <DesktopSideBar />
        <Flex
          ref={ref}
          direction={"column"}
          width={"100%"}
          height={`calc(100vh - ${pxToRem(120)})`}
          minHeight={"unset"}
          overflow={"auto"}
          onScroll={() => {
            if (ref.current)
              setHeader(ref.current.scrollTop, ref.current.scrollHeight);
          }}
          css={{
            "&::-webkit-scrollbar": {
              width: "6px",
            },
            "&::-webkit-scrollbar-track": {
              width: "6px",
            },
            "&::-webkit-scrollbar-thumb": {
              background: "rgba(0, 0, 0, 0.4)",
              borderRadius: "24px",
            },
          }}
        >
          <Header cb={headerCallBack} />
          <CustomSuspense
            fallback={
              <Box pos={"relative"} top={"30%"} textAlign={"center"}>
                <Spinner />
              </Box>
            }
            state={spotifyAuthLodableState}
          >
            <Outlet />
          </CustomSuspense>
        </Flex>
      </Flex>
      <MusixPlayer />
      <Box
        w={"100%"}
        height={pxToAll(20)}
        bg={"shade.hoverPrimary"}
        pos={"fixed"}
        bottom={"0"}
        zIndex={"1"}
        textAlign="right"
        px={pxToAll(10)}
      >
        <Text fontSize={pxToAll(12)}>
          <Link to="privacy">Privacy Policy</Link> &copy; 2022 Contents - Musix
          for all
        </Text>
      </Box>
      <AgentDetect
        mobileComponent={<MobileSideBar />}
        desktopComponent={<Box />}
      />
    </Flex>
  );
}

function NetworkNotify() {
  const { isOnline } = useNetworkAgent();
  let text = "";
  if (typeof isOnline === "boolean") {
    text = isOnline
      ? "Connecting..."
      : "There is some problem connnecting with the internet.Please check your network...";
  }
  return (
    <Box
      h={pxToAll(isOnline === "" ? 0 : 20)}
      textAlign="center"
      bg="green.500"
      transition={"height 0.25s ease"}
    >
      <Text
        fontSize={pxToAll(14.5)}
        color="text.secondary"
        opacity={isOnline === "" ? 0 : 1}
        transition={"opacity 0.5s ease"}
      >
        {text}
      </Text>
    </Box>
  );
}
