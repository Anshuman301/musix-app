import { Box, Flex, Text } from "@chakra-ui/react";
import { useRecoilValue } from "recoil";
import { albumPlayListTrackState } from "../atoms/albumPlayList.atom.js";
import { openFullPlayerState } from "../components/player/MobileMusixPlayer.jsx";
import Track from "../components/track/Track.jsx";
import { PLAYMODE } from "../utils/constants/trackState.constants.js";
import { pxToAll } from "../utils/theme.utils.js";

export default function QueuePage() {
  const isOpenFullPlayer = useRecoilValue(openFullPlayerState);
  const albumPlayListTrack = useRecoilValue(albumPlayListTrackState);
  const playedTrack = albumPlayListTrack?.items?.find(
    (item) =>
      item?.isPlaying === PLAYMODE.PLAYING || item?.isPlaying === PLAYMODE.PAUSE
  );
  if (!albumPlayListTrack || !playedTrack) {
    return (
      <Box
        pos={"relative"}
        height="100%"
        textAlign={"center"}
        top={"30%"}
        pl={pxToAll(20)}
        pr={pxToAll(15)}
      >
        <Text textStyle={"h4"} color={"text.disabled"}>
          No listening songs !!! right now{" "}
        </Text>
        <Text textStyle={"label"} color={"text.disabled"}>
          Try playing songs
        </Text>
      </Box>
    );
  }
  return (
    <Flex
      direction={"column"}
      pl={pxToAll(20)}
      pr={pxToAll(15)}
      pb={pxToAll(15)}
      height={isOpenFullPlayer ? "55%" : "auto"}
      zIndex={isOpenFullPlayer ? "4" : "unset"}
      overflow={isOpenFullPlayer ? "hidden auto" : "initial"}
    >
      <Text textStyle={"h4"} color="text.secondary">
        Listening play track
      </Text>
      <Text
        textStyle={"h6"}
        color="text.play"
        fontWeight={"bold"}
        mt={pxToAll(10)}
      >
        Now playing
      </Text>
      <Flex
        mt={pxToAll(10)}
        overflow={isOpenFullPlayer ? "initial" : "hidden"}
        direction="column"
      >
        <Track
          header={{ id: albumPlayListTrack.id, type: albumPlayListTrack.type }}
          id={playedTrack.itemId}
          artists={playedTrack.artists}
          album={playedTrack.album}
          name={playedTrack.song}
          duration_ms={playedTrack.duration}
        />
      </Flex>
      <Box mt={pxToAll(20)} />
      <Text textStyle={"h6"} color={"text.play"} fontWeight={"bold"}>
        All track list
      </Text>
      <Flex
        mt={pxToAll(10)}
        overflow={isOpenFullPlayer ? "initial" : "hidden"}
        direction="column"
      >
        {albumPlayListTrack.items.map((track, idx) => (
          <Track
            key={`queue-${track.itemId}`}
            header={{
              id: albumPlayListTrack.id,
              type: albumPlayListTrack.type,
            }}
            id={track.itemId}
            artists={track.artists}
            album={track.album}
            name={track.song}
            duration_ms={track.duration}
            seq={idx + 1}
          />
        ))}
      </Flex>
    </Flex>
  );
}
