import {
  Box,
  Divider,
  Flex,
  Grid,
  GridItem,
  HStack,
  Icon,
  IconButton,
  Spinner,
  Text,
} from "@chakra-ui/react";
import { FaPause, FaPlay } from "react-icons/fa";
import { FcLike } from "react-icons/fc";
import { GiSelfLove } from "react-icons/gi";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import {
  useRecoilState,
  useRecoilValue,
  useRecoilValueLoadable,
  useResetRecoilState,
} from "recoil";
import {
  albumPlaylistParamState,
  likedItemsState,
} from "../atoms/albumPlayList.atom.js";
import Track from "../components/track/Track.jsx";
import CustomSuspense from "../components/util/CustomSuspense";
import useLiked from "../hooks/useLiked.js";
import usePlayPauseClick from "../hooks/usePlayPauseClick.js";
import { albumPlayListDetailsSate } from "../selector/albumPlayList.selector.js";
import ROUTER, {LIKED_SONGS, TOP_LISTEN} from "../utils/constants/router.constants.js";
import { pxToAll, pxToRem, pxToRemSm, showOut } from "../utils/theme.utils.js";
import { authState } from "../atoms/auth.atoms.js";
import useCustomToast from "../hooks/useCustomToast.js";
import { Fragment, useEffect } from "react";
import useAgent from "../hooks/useAgent.js";

export default function AlbumPlayListPage() {
  const location = useLocation();
  const params = useParams();
  const [albumPlaylistParam, setAlbumPlaylistParam] = useRecoilState(
    albumPlaylistParamState
  );
  const navigate = useNavigate();
  const auth = useRecoilValue(authState);
  const resetParam = useResetRecoilState(albumPlaylistParamState);
  useEffect(() => {
    if (
      location.pathname.includes(`${ROUTER.PLAYLIST}`) &&
      params["playlistId"]
    )
      setAlbumPlaylistParam({
        type: ROUTER.PLAYLIST,
        id: params["playlistId"],
      });
    else if (location.pathname.includes(`${ROUTER.ALBUM}`) && params["albumId"])
      setAlbumPlaylistParam({
        type: ROUTER.ALBUM,
        id: params["albumId"],
      });
    else if (location.pathname.includes(ROUTER.UPLAYLIST) && params["playlistId"]) {
      if (auth.isAuth)
        setAlbumPlaylistParam({
          type: ROUTER.UPLAYLIST,
          id: params["playlistId"],
        });
      else navigate("/");
    } else if (location.pathname.includes(ROUTER.COLLECTIONS.setSubRoute(LIKED_SONGS))) {
      if (auth.isAuth)
        setAlbumPlaylistParam({
          type: ROUTER.COLLECTIONS.name,
          id: LIKED_SONGS,
        });
      else navigate("/");
    }else if (location.pathname.includes(ROUTER.COLLECTIONS.setSubRoute(TOP_LISTEN))) {
      if (auth.isAuth)
        setAlbumPlaylistParam({
          type: ROUTER.COLLECTIONS.name,
          id: TOP_LISTEN,
        });
      else navigate("/");
    }
    return () => {
      resetParam();
    };
  }, [location.pathname]);
  if (albumPlaylistParam?.type === ROUTER.PLAYLIST) return <PlayListPage />;
  else if (albumPlaylistParam?.type === ROUTER.ALBUM) return <AlbumPage />;
  else if (albumPlaylistParam?.type === ROUTER.UPLAYLIST) return <PlayListPage />;
  else if (albumPlaylistParam?.type === ROUTER.COLLECTIONS.name) return <PlayListPage />;
  return null;
}

function PlayListPage() {
  const albumPlaylistParam = useRecoilValue(albumPlaylistParamState);
  const albumPlayListLoadable = useRecoilValueLoadable(
    albumPlayListDetailsSate(albumPlaylistParam)
  );
  const isMobile = useAgent();
  const contents = albumPlayListLoadable?.contents;
  const imgUrl = contents?.images?.[0]?.url || contents?.imgUrl;
  const type = contents?.type;
  const name = contents?.name;
  const desc = contents?.description;
  let filteredTracks = contents?.tracks?.items || contents?.tracks || [];
  if (type === ROUTER.PLAYLIST)
    filteredTracks = filteredTracks.filter((content) => !!content.track);
  return (
    <CustomSuspense
      state={albumPlayListLoadable.state}
      fallback={
        <Box
          children={<Spinner />}
          pos={"relative"}
          top={"30%"}
          textAlign={"center"}
          height={"100%"}
        />
      }
    >
      <Flex direction={"column"} pos={"relative"} top={pxToAll(-80)}>
        <AlbumPlaylistHeaderContent
          url={imgUrl}
          type={type}
          name={name}
          desc={desc}
        />
        <Flex
          direction={"column"}
          pl={pxToAll(20)}
          pr={isMobile ? pxToRem(20) : ["0", "0", pxToRem(20)]}
          overflow={"hidden"}
        >
          <AlbumPlaylistMiddleContent
            id={contents?.id}
            type={type}
            title={name}
            imageSource={imgUrl}
            details={{ description: desc }}
          />
          <Grid
            templateColumns={"25px minmax(170px, 2fr) minmax(85px, 1fr) 70px"}
            gridColumnGap={pxToAll(20)}
            height={pxToAll(40)}
            alignItems={"center"}
            px={pxToAll(20)}
          >
            <GridItem justifySelf={"end"}>
              <Text textStyle={"label"}>#</Text>
            </GridItem>
            <GridItem>
              <Text textStyle={"label"} letterSpacing={"widest"}>
                TITLE
              </Text>
            </GridItem>
            <GridItem>
              <Text textStyle={"label"} isTruncated letterSpacing={"widest"}>
                ALBUM
              </Text>
            </GridItem>
            <GridItem justifySelf={"end"} letterSpacing={"widest"}>
              <Text textStyle={"label"}>DURATION</Text>
            </GridItem>
          </Grid>
          <Divider orientation="horizontal" colorScheme={"teal"} />
          {filteredTracks.map((props, idx) => {
            const track = props?.track || props;
            const { id, ...rest } = track;
            return (
              <Track
                {...rest}
                key={id}
                id={id}
                seq={idx + 1}
                header={{ id: contents?.id, name, desc, type, imgUrl }}
              />
            );
          })}
        </Flex>
      </Flex>
    </CustomSuspense>
  );
}

function AlbumPage() {
  const albumPlaylistParam = useRecoilValue(albumPlaylistParamState);
  const albumPlayListLoadable = useRecoilValueLoadable(
    albumPlayListDetailsSate(albumPlaylistParam)
  );
  const isMobile = useAgent();
  const contents = albumPlayListLoadable.contents;
  const imgUrl = contents?.images?.[0]?.url;
  const type = contents?.type;
  const name = contents?.name;
  const artists = contents?.artists ?? [];
  const tracks = contents?.tracks?.items ?? [];
  return (
    <CustomSuspense
      state={albumPlayListLoadable.state}
      fallback={
        <Box
          children={<Spinner />}
          pos={"relative"}
          top={"30%"}
          textAlign={"center"}
          height={"100%"}
        />
      }
    >
      <Flex direction={"column"} pos={"relative"} top={pxToAll(-80)}>
        <AlbumPlaylistHeaderContent
          url={imgUrl}
          type={type}
          name={name}
          artists={artists}
        />
        <Flex
          direction={"column"}
          pl={pxToAll(20)}
          pr={isMobile ? pxToRem(20) : ["0", "0", pxToRem(20)]}
          overflow={"hidden"}
        >
          <AlbumPlaylistMiddleContent
            id={contents?.id}
            type={type}
            imageSource={imgUrl}
            title={name}
            details={{ artists }}
          />
          <Grid
            templateColumns={"25px minmax(190px, 1fr) 70px"}
            gridColumnGap={pxToAll(20)}
            height={pxToAll(40)}
            alignItems={"center"}
            px={pxToAll(20)}
          >
            <GridItem justifySelf={"end"}>
              <Text textStyle={"label"} letterSpacing={"widest"}>
                #
              </Text>
            </GridItem>
            <GridItem>
              <Text textStyle={"label"} letterSpacing={"widest"}>
                TITLE
              </Text>
            </GridItem>
            <GridItem justifySelf={"end"}>
              <Text textStyle={"label"} letterSpacing={"widest"}>
                DURATION
              </Text>
            </GridItem>
          </Grid>
          <Divider orientation="horizontal" colorScheme={"teal"} />
          {tracks.map(({ id, ...rest }, idx) => {
            let headerId = contents?.id;
            return (
              <Track
                {...rest}
                key={id}
                id={id}
                seq={idx + 1}
                header={{ id: headerId, name, artists, type, imgUrl }}
              />
            );
          })}
        </Flex>
      </Flex>
    </CustomSuspense>
  );
}

function AlbumPlaylistHeaderContent({ url, type, name, ...rest }) {
  const desc = rest?.desc ?? "";
  return (
    <Fragment>
      <Box
        bgImage={url}
        bgSize={"cover"}
        bgPos={["center"]}
        height={[pxToRemSm(170), null, pxToRem(380)]}
        width={"100%"}
        animation={`${showOut} 0.5s linear 1 normal`}
      />
      <Box
        bgGradient={
          "linear(to-t, blackAlpha.700 10%,blackAlpha.600 30%, transparent 100%)"
        }
        height={[
          `calc(${pxToRemSm(170)} - ${pxToRemSm(53.3)})`,
          null,
          `calc(${pxToRem(380)} - ${pxToRem(80)})`,
        ]}
        width={"100%"}
        zIndex={1}
        pos={"absolute"}
        top={pxToAll(80)}
        animation={`${showOut} 0.5s linear 1 normal`}
      />
      <Flex
        p={pxToAll(20)}
        width={"100%"}
        position={"absolute"}
        top={pxToAll(80)}
        direction={"column"}
        justifyContent={"flex-end"}
        height={[
          `calc(${pxToRemSm(170)} - ${pxToRemSm(53.3)})`,
          null,
          `calc(${pxToRem(380)} - ${pxToRem(80)})`,
        ]}
        zIndex={2}
      >
        <Text
          textTransform={"uppercase"}
          textStyle={"h6"}
          color={"text.secondary"}
          fontWeight={"bold"}
        >
          {type}
        </Text>
        <Text
          textStyle={"h2"}
          color={"text.secondary"}
          lineHeight={"1.25"}
          isTruncated
        >
          {name}
        </Text>
        <Text textStyle={"label"} isTruncated>
          {desc}
        </Text>
      </Flex>
    </Fragment>
  );
}

export function AlbumPlaylistMiddleContent({
  id,
  type,
  title,
  imageSource,
  details,
}) {
  const { isPlaying, isLoading, handlePlayClick, handlePauseClick } =
    usePlayPauseClick(id);
  const onLiked = useLiked();
  const toast = useCustomToast();
  const auth = useRecoilValue(authState);
  const likedItems = useRecoilValue(likedItemsState);
  const location = useLocation();
  const showLikeButton = location.pathname.includes(`/${ROUTER.COLLECTIONS.name}`);
  const isLiked = !!likedItems?.[id] && likedItems?.[id] === type;
  const handleClick = () => {
    if (auth.isAuth) {
      if (!isLoading) {
        if (isPlaying) handlePauseClick();
        else handlePlayClick(type, title, imageSource, details);
      }
    } else toast();
  };

  return (
    <HStack spacing={pxToAll(20)} my={pxToAll(20)}>
      <IconButton
        aria-label="play-pause-butt"
        size={"xl"}
        isLoading={isLoading}
        icon={isPlaying ? <FaPause /> : <FaPlay />}
        onClick={handleClick}
      />
      {!showLikeButton && (
        <Icon
          as={isLiked ? FcLike : GiSelfLove}
          textStyle={"icon.lg"}
          _hover={{
            color: "text.secondary",
            transform: "scale(1.1)",
            transition: "all 0.25s",
          }}
          onClick={() => {
            if (auth.isAuth) onLiked(id, type);
            else toast();
          }}
        />
      )}
    </HStack>
  );
}
