import { Box, Flex, GridItem, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useRecoilValueLoadable } from "recoil";
import CardRenderer from "../components/cardRenderrer";
import ContentWrapper, {
  BigCardWrapper,
} from "../components/ContentWrapper/ContentWrapper";
import { searchDetailsState } from "../components/Input/SearchInput";
import Track from "../components/track/Track";
import CustomSuspense from "../components/util/CustomSuspense";
import useAgent from "../hooks/useAgent";
import type { paramsType } from "../selector/content.selector";
import ROUTER from "../utils/constants/router.constants.js";
import { pxToAll } from "../utils/theme.utils.js";

export default function SearchTextContentPage() {
  const state = useLocation().state as paramsType;
  const searchText = state?.search?.trim() ?? "";
  const [topResult, setTopResult] = useState({});
  const searchDetailsLoadable = useRecoilValueLoadable(
    searchDetailsState(searchText)
  );
  const isMobile = useAgent();
  const contents = searchDetailsLoadable.contents;
  const tracks = contents?.tracks?.items ?? [];
  const albums = contents?.albums;
  const playlists = contents?.playlists;
  const artistItems = contents?.artists?.items ?? [];
  useEffect(() => {
    if (searchDetailsLoadable.state === "hasValue") {
      const artistTrackItems = [...artistItems, ...tracks];
      const allItems = [
        ...artistTrackItems,
        ...albums?.items,
        ...playlists?.items,
      ];
      let matchedItem = allItems.find(
        (item) => item.name.toLowerCase() === searchText.toLowerCase()
      );
      if (!matchedItem) {
        const popularityArr = artistTrackItems.map((item) => item.popularity);
        const maxPop = Math.max(...popularityArr);
        const idx = popularityArr.findIndex((item) => item === maxPop);
        matchedItem = artistTrackItems[idx];
      }
      setTopResult(matchedItem);
    }
  }, [searchDetailsLoadable.state]);
  return (
    <CustomSuspense state={searchDetailsLoadable.state} fallback={<Box />}>
      <Flex direction={"column"} pb={pxToAll(15)}>
        <CardRenderer
          autoRows={"auto"}
          minCardWidth={"280"}
          rowGap={"0.7rem"}
          noOfChildren={"auto-fit"}
          overflowX="hidden"
        >
          <GridItem>
            <Text textStyle={"h5"} color="text.secondary" mb={pxToAll(15)}>
              Top Result
            </Text>
            <BigCardWrapper
              {...topResult}
              width={pxToAll(170)}
              cardHeight={"max-content"}
            />
          </GridItem>
          <GridItem>
            <Text textStyle={"h5"} color="text.secondary">
              Songs
            </Text>
            {tracks.map(({ id, ...rest }, idx: number) => {
              const { album, artists } = rest;
              return (
                <Track
                  {...rest}
                  key={id}
                  id={id}
                  seq={idx + 1}
                  header={{
                    id: album.id,
                    name: album.name,
                    artists,
                    type: ROUTER.ALBUM,
                    imgUrl: album.images[0].url,
                  }}
                />
              );
            })}
          </GridItem>
        </CardRenderer>
        <ContentWrapper
          albums={albums}
          message={"Albums"}
          seeAll={false}
          noOfChildren={isMobile ? 5 : "auto-fill"}
        />
        <ContentWrapper
          playlists={playlists}
          message={"Playlists"}
          seeAll={false}
          noOfChildren={isMobile ? 5 : "auto-fill"}
        />
        <ContentWrapper
          rows={artistItems}
          message={"Artists"}
          seeAll={false}
          noOfChildren={isMobile ? 5 : "auto-fill"}
        />
      </Flex>
    </CustomSuspense>
  );
}
