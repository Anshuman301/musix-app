import { Box, Flex, keyframes, Spinner, Text } from "@chakra-ui/react";
import { Fragment } from "react";
import { useParams } from "react-router-dom";
import { useRecoilValueLoadable } from "recoil";
import ContentWrapper from "../components/ContentWrapper/ContentWrapper";
import Track from "../components/track/Track";
import CustomSuspense from "../components/util/CustomSuspense";
import useAgent from "../hooks/useAgent";
import { artistInfoSelector } from "../selector/artist.selector";
import { pxToRemSm, pxToRem, pxToAll, showOut } from "../utils/theme.utils";
import { AlbumPlaylistMiddleContent } from "./AlbumPlayListPage";

export default function ArtistPage() {
  const artistId = useParams().artistId as string;
  const isMobile = useAgent();
  const { contents, state } = useRecoilValueLoadable(
    artistInfoSelector(artistId)
  );
  const artistInfo = contents[0]?.[0] ?? null;
  const topTracks = contents[1]?.[0]?.tracks ?? [];
  const artistAlbumItems = contents[2]?.[0]?.items ?? [];
  const relatedArtists = contents[3]?.[0]?.artists ?? [];

  return (
    <CustomSuspense
      state={state}
      fallback={
        <Box textAlign={"center"} pos={"relative"} height={"100%"} top={"30%"}>
          <Spinner />
        </Box>
      }
    >
      <Fragment>
        <Box
          bgImage={artistInfo?.images?.[0]?.url}
          bgSize={"cover"}
          bgRepeat={"no-repeat"}
          bgPos={"top"}
          width={
            isMobile
              ? `calc(100vw - 6px)`
              : [
                  `calc(100vw - 6px - ${pxToRemSm(230 / 1.5)})`,
                  null,
                  `calc(100vw - 6px - ${pxToRem(230)})`,
                ]
          }
          position={"fixed"}
          height={[
            `calc(${pxToRemSm(80 / 1.5)} + 60% + ${pxToRemSm(160 / 1.5)})`,
            null,
            `calc(${pxToRem(80)} + 60% + ${pxToRem(160)})`,
          ]}
          animation={`${showOut} 0.5s linear 1 normal`}
        />
        <Flex
          direction={"column"}
          pos={"relative"}
          top={"30%"}
          bgGradient={
            "linear-gradient(to top, var(--chakra-colors-brand-secondary) 60%, rgb(44 82 130 / 85%) 80%, var(--chakra-colors-blackAlpha-500) 90%, var(--chakra-colors-blackAlpha-400) 95%, var(--chakra-colors-transparent) 100%)"
          }
          px={pxToAll(20)}
          pb={pxToAll(20)}
        >
          <Text textStyle={"h2"} color={"text.secondary"} isTruncated>
            {artistInfo?.name}
          </Text>
          <Flex
            direction={"column"}
            pr={isMobile ? pxToRem(20) : ["0", "0", pxToRem(20)]}
            overflow={"hidden"}
          >
            <AlbumPlaylistMiddleContent
              id={artistId}
              imageSource={artistInfo?.images?.[0]?.url}
              title={artistInfo?.name}
              type={"artist"}
              details={{ artists: [artistInfo] }}
            />
            {topTracks.map(({ id, ...rest }, idx: number) => (
              <Track
                {...rest}
                key={id}
                id={id}
                seq={idx + 1}
                header={{
                  id: artistId,
                  name: artistInfo.name,
                  type: "artist",
                  artists: [artistInfo],
                  imgUrl: artistInfo?.images?.[0]?.url,
                }}
              />
            ))}
          </Flex>
          {artistAlbumItems.length !== 0 && (
            <Box mt={pxToAll(20)}>
              <ContentWrapper
                key={`${artistId} - albums`}
                seeAll={false}
                noOfChildren={isMobile ? 6 : "auto-fill"}
                rows={artistAlbumItems}
                message={`Albums`}
              />
            </Box>
          )}
          {relatedArtists.length != 0 && (
            <Box mt={pxToAll(20)}>
              <ContentWrapper
                key={`${artistId} - related artist`}
                seeAll={false}
                noOfChildren={isMobile ? 6 : "auto-fill"}
                rows={relatedArtists}
                message={`Related artist`}
              />
            </Box>
          )}
        </Flex>
      </Fragment>
    </CustomSuspense>
  );
}
