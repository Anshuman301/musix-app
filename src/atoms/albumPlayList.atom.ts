import { atom } from "recoil";
import type { paramsType } from "../selector/content.selector";
import { PLAYMODE } from "../utils/constants/trackState.constants";
export interface ArtistType {
  name: string;
  id: string;
}
export type IsPlayingType = 0 | 1 | 2 | -1;
export interface ItemType {
  itemId: string;
  isPlaying: IsPlayingType;
  song: string;
  album: string;
  duration: string;
  artists: ArtistType[];
  imageSource: string;
}
export interface albumPlayListTrackType {
  id: string;
  items: ItemType[];
  type: string;
  isPlaying: IsPlayingType;
}
export interface ModAlbumPlaylistSelType {
  nextIdx: number;
  nextSearchTrack: string;
  currentItem: ItemType;
  prevIdx: number;
  totalLength: number;
  prevSearchTrack: string;
  id: string;
  searchTrack: string;
  type: string;
  albumPlayListId: string;
  currIdx: number;
}
export const searchTrackState = atom({
  key: "searchTrackState",
  default: "",
});
const getInitAlbumPlayListTrack = (initState: albumPlayListTrackType) => {
  const keyLength = Object.keys(initState).length;
  if ([0, 1].includes(keyLength)) return initState;
  const itemFound = initState.items.find(
    (item) =>
      item.isPlaying === PLAYMODE.PLAYING ||
      item.isPlaying === PLAYMODE.PAUSE ||
      item.isPlaying === PLAYMODE.LOADING
  );
  if (itemFound) {
    itemFound.isPlaying = PLAYMODE.PAUSE;
  }
  return initState;
};
export const albumPlayListTrackState = atom({
  key: "albumPlayListTrackState",
  default: getInitAlbumPlayListTrack(
    JSON.parse(
      localStorage.getItem("albumPlayListTrackState") || "{}"
    ) as albumPlayListTrackType
  ),
});

export const albumPlaylistParamState = atom({
  key: "albumPlaylistParamState",
  default: {} as paramsType,
});

export const likedItemsState = atom({
  key: "likedItemsState",
  default: {} as paramsType,
});
