import { atom } from "recoil";
import type { paramsType } from "../selector/content.selector";
export const genreParamState = atom({
  key: "genreParamState",
  default: null as paramsType | null,
});
