import { atom } from "recoil";
interface authType {
  isAuth: -1 | boolean;
  signIn: undefined | Function;
}
export const authState = atom({
  key: "authState",
  default: {
    isAuth: -1,
    signIn: undefined,
  } as authType,
});
