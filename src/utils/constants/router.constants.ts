const ROUTER = {
  HOME: "/",
  SEARCH: "search",
  GENRE: "genre",
  PLAYLIST: "playlist",
  ALBUM: "album",
  ARTIST: "artist",
  QUEUE: "queue",
  UPLAYLIST: "uPlaylist",
  COLLECTIONS: {
    name: "collection",
    setSubRoute(sub){
      return `${this.name}/${sub}`;
    }
  }
};

export const LIKED_SONGS = "liked";

export const TOP_LISTEN = "top-listens";

export default ROUTER;
