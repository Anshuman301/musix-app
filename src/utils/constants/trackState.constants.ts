export const PLAYMODE: { [key: string]: 0 | 1 | 2 | -1 } = {
  PAUSE: 0,
  PLAYING: 1,
  INQUEUE: 2,
  LOADING: -1,
};

export const REPMODE = {
  NONE: "0",
  ALL: "00",
  ONCE: "000",
};
