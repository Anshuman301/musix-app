import { extractContentByTime } from "./date.utils.js";
import type { paramsType } from "../selector/content.selector.js";
import categoryJSON from "../utils/constants/category.constant.json"
const categoryItems = categoryJSON.categories.items
export const content = [
  "Top Lists",
  ["Workout", "Cooking & Dining", "Sleep"],
  "Focus",
  "Travel",
  "API Heritage Month",
  { as: "release", property: "release" },
  { as: "featured", property: "featured" },
  "Soul",
];

export const authContent = [
  "Top Lists",
  ["Workout", "Cooking & Dining", "Sleep"],
  { as: "release", property: "release" },
  { as: "featured", property: "featured" },
  ["Focus", "API Heritage Month", "Soul"],
];

export const getCategoryDetailsMap = () => {
  const categoryMap = new Map<string, typeof categoryItems[0]>()
  categoryItems.forEach(item => {
    categoryMap.set(item.name, item)
  })
  return categoryMap;
}
export function showContentConversionUtil(contents: typeof content) {
  const categoryMap = getCategoryDetailsMap();
  const modContents = contents.map(content => {
    if(typeof content === 'string')
    return categoryMap.get(content)?.id
    if(Array.isArray(content))
    return content.map(ele => categoryMap.get(ele)?.id)
    return content
  })
  return modContents.map((item) => {
    if (typeof item === "string") return { as: "category", property: item };
    else if (Array.isArray(item))
      return { as: "category", property: extractContentByTime(item) };
    else return item;
  });
}

export function parseDurationIntoSec(duration) {
  const minMatch = duration.match(/[0-9]+M/);
  const secMatch = duration.match(/[0-9]+S/);
  const min = minMatch ? parseInt(minMatch[0]) : 0;
  const sec = secMatch ? parseInt(secMatch[0]) : 0;
  return min * 60 + sec;
}

export const searchTrackTemplate = (song, artist) =>
  `song ${song} by ${artist} audio lyrics`;

export const secondsToMins = (sec) =>
  `${Math.floor(sec / 60)}:${("0" + (Math.floor(sec) % 60)).slice(-2)}`;

/**
 * @description Use Only for getting new Array value for setAlbumPlayListTrack play/pause
 * @param prevState @param value @param index
 * @returns newState
 */
export const getNewStateForPlayPause = (prevState, value, index) => ({
  ...prevState,
  isPlaying: value,
  items: [
    ...prevState.items.slice(0, index),
    {
      ...prevState.items[index],
      isPlaying: value,
    },
    ...prevState.items.slice(index + 1),
  ],
});

/**
 * @description Use Only for repeat Track for prev,next track
 * @param prevState @param totalLength @param playMode1 @param playMode2
 * @returns newState
 */
export const getNewStateForRepeatPlayBack = (
  prevState,
  totalLength,
  playMode1,
  playMode2
) => ({
  ...prevState,
  items: [
    {
      ...prevState.items[0],
      isPlaying: playMode1,
    },
    ...prevState.items.slice(1, totalLength - 1),
    {
      ...prevState.items[totalLength - 1],
      isPlaying: playMode2,
    },
  ],
});

export const isSamePath = (prevPath: string, newPath: string): boolean => {
  let prevArr = prevPath?.match(/[a-zA-Z]+/gi) ?? [];
  let newArr = newPath?.match(/[a-zA-Z]+/gi) ?? [];
  if (prevArr.length !== newArr.length) return false;
  return prevArr.every((ele, idx) => ele === newArr[idx]);
};

export const debounce = (fn: Function, time: number) => {
  let timer: number | undefined = undefined;
  return function (...args) {
    clearInterval(timer);
    timer = setTimeout(() => {
      fn(...args);
    }, time);
  };
};

export const handleImgResize = (imgRefcurr: HTMLImageElement) => {
  if (imgRefcurr.width !== imgRefcurr.height) {
    imgRefcurr.style.height = `${imgRefcurr.width}px`;
  }
};
