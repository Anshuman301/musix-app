import { spotifyAxios } from "./axios.utils.js";
let spotifyToken = "",
  currentTime: number | null = null;
export async function spotifyAuth(callingCheck = false) {
  let error = undefined;
  if (callingCheck && currentTime) {
    if (currentTime + 3600 * 1000 > new Date().getTime())
      return [spotifyToken, error];
  }
  try {
    const resp = await spotifyAxios(
      `${import.meta.env.SNOWPACK_PUBLIC_REACT_APP_SPOTIFY_AUTH}`
    ).post(
      "https://accounts.spotify.com/api/token?grant_type=client_credentials",
      null,
      {
        baseURL: "",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
    spotifyToken = "Bearer " + resp.data.access_token;
    currentTime = new Date().getTime();
  } catch (e: any) {
    error = e.toString();
  }
  return [spotifyToken, error];
}

const createSpotifyAxiosInstance = async (
  url: string,
  query: { [key: string]: string }
) => {
  let data: undefined | { [key: string]: object } = undefined,
    error = undefined;
  try {
    const resp = await spotifyAxios(spotifyToken).get(url, {
      params: query,
    });
    data = resp.data;
  } catch (e: any) {
    error = e.toString();
  }
  return [data, error];
};

export const getNewReleases = async (query) =>
  await createSpotifyAxiosInstance("/browse/new-releases", query);

export const getSeveralCategories = async (query) =>
  await createSpotifyAxiosInstance("/browse/categories", query);

export const getCategoryDetails = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/browse/categories/${id}`, query);

export const getFeaturedPlayList = async (query) =>
  await createSpotifyAxiosInstance("/browse/featured-playlists", query);

export const getCategoryPlayList = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/browse/categories/${id}/playlists`, query);

export const getPlayListDetails = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/playlists/${id}`, query);

export const getAlbum = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/albums/${id}`, query);

export const getAlbumTracks = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/albums/${id}/tracks`, query);

export const getArtists = async (query) =>
  await createSpotifyAxiosInstance("/artists", query);

export const getArtist = async (id: string) =>
  await createSpotifyAxiosInstance(`/artists/${id}`, {});
export const getArtistAlbums = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/artists/${id}/albums`, query);

export const getArtistTopTracks = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/artists/${id}/top-tracks`, query);

export const getRelatedArtists = async (id: string, query) =>
  await createSpotifyAxiosInstance(`/artists/${id}/related-artists`, query);

export const getTracks = async (query) =>
  await createSpotifyAxiosInstance("/tracks", query);

export const searchItems = async (query) =>
  await createSpotifyAxiosInstance("/search", query);

export const recommend = async (query) =>
  await createSpotifyAxiosInstance("/recommendations", query);
