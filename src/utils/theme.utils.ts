import { keyframes } from "@emotion/react";

export const pxToRem = (px: number) => {
  const remValue = (px * 100) / (1080 + 1024 + 512);
  return `${remValue}rem`;
};

export const pxToRemSm = (px: number) => {
  const remValue = (px * 100) / (375 + 507 + 187.5);
  return `${remValue}rem`;
};

export const pxToRemMd = (px: number) => {
  const remValue = (px * 100) / (768 + 1024 + 384);
  return `${remValue}rem`;
};

export const pxToRemLg = (px: number) => {
  const remValue = (px * 100) / (1024 + 507 + 253.5);
  return `${remValue}rem`;
};

export const pxToRemXl = (px: number) => {
  const remValue = (px * 100) / (1440 + 596 + 298);
  return `${remValue}rem`;
};
const funcArr = (px: number) => [pxToRemSm(px / 1.5), null, pxToRem(px)];

export const pxToAll = (px: number, defaultValue = 2) => {
  return funcArr(px);
};

export const showOut = keyframes`from {
  opacity:0.1
}
to {
  opacity:1
}`;
