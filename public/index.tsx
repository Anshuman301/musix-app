/* Add JavaScript code here! */
import App from "../src/App";
import ReactDOM from "react-dom";
import { ChakraProvider, ColorModeScript } from "@chakra-ui/react";
import { RecoilRoot, useRecoilSnapshot } from "recoil";
import theme from "../src/theme/index.theme.js";
import { useEffect } from "react";
function DebugObserver() {
  const snapshot = useRecoilSnapshot();
  useEffect(() => {
    if (!(import.meta.env.MODE === "production")) {
      console.debug("The following atoms were modified:");
      for (const node of snapshot.getNodes_UNSTABLE({ isModified: true })) {
        console.debug(node.key, snapshot.getLoadable(node));
      }
    }
  }, [snapshot]);

  return null;
}
ReactDOM.render(
  <RecoilRoot>
    <DebugObserver />
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <App />
    </ChakraProvider>
  </RecoilRoot>,
  document.getElementById("root")
);
if (import.meta.env.MODE === "production") {
  if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
      navigator.serviceWorker
        .register("/service-worker.js")
        .then((registration) => {
          console.debug("SW registered: ", registration);
        })
        .catch((registrationError) => {
          console.error("SW registration failed: ", registrationError);
        });
    });
  }
}
