/** @type {import("snowpack").SnowpackUserConfig } */
import WorkboxPlugin from "workbox-webpack-plugin";
export default {
  exclude: ["**/node_modules/**/*", ".vscode"],
  mount: {
    public: { url: "/" },
    src: { url: "/dist" },
  },
  plugins: [
    "@snowpack/plugin-dotenv",
    "@snowpack/plugin-typescript",
    [
      "@snowpack/plugin-webpack",
      {
        manifest: true,
        extendConfig: (config) => {
          config.plugins.push(
            new WorkboxPlugin.GenerateSW({
              // these options encourage the ServiceWorkers to get in there fast
              // and not allow any straggling "old" SWs to hang around
              clientsClaim: true,
              skipWaiting: true,
            })
          );
          return config;
        },
      },
    ],
  ],
  routes: [
    {
      match: "routes",
      src: ".*",
      dest: "/index.html",
    },
  ],
  optimize: {
    /* Example: Bundle your final build: */
    // "bundle": true,
  },
  packageOptions: {
    /* ... */
  },
  devOptions: {},
  buildOptions: {
    clean: true,
    jsxInject: `import React from 'react'`,
  },
};
